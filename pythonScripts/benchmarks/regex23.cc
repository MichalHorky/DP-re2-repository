#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:Regex.Replace(text, @\".+[a-zA-Z0-9]{17,500}.+(\r\n)\", Environment.NewLine, RegexOptions.Compiled))", opt);
    } else {
        RE2 pattern("Regex.Replace(text, @\".+[a-zA-Z0-9]{17,500}.+(\r\n)\", Environment.NewLine, RegexOptions.Compiled)", opt);
    }
}