#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:function parameterizedHtml(match, p1, p2)\", \"([a-f0-9]{6}).*([a-f0-9]{6})\", \"([a-f0-9]{3}|[a-f0-9]{6}).*([a-f0-9]{3}|[a-f0-9]{6}))", opt);
    } else {
        RE2 pattern("function parameterizedHtml(match, p1, p2)\", \"([a-f0-9]{6}).*([a-f0-9]{6})\", \"([a-f0-9]{3}|[a-f0-9]{6}).*([a-f0-9]{3}|[a-f0-9]{6})", opt);
    }
}