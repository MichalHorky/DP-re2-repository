#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:rise (\\d{4}) set (\\d{4}).*(\\d{2})h (\\d{2})m (\\d{2})s - (\\d)m (\\d{2})s (\\d{3})\", \"rise \\d{4} set \\d{4}.*\\d{2}h \\d{2}m \\d{2}s - \\dm \\d{2}s \\d{3}\", \"rise (\\d*) set (\\d*) .* (\\d*)h (\\d*)m (\\d*)s - (\\d*)m (\\d*)s (\\d*)\", \"rise \\d{4} set \\d{4} \\(:\\) \\d{2}h \\d{2}m \\d{2}s - \\dm \\d{2}s \\d{3})", opt);
    } else {
        RE2 pattern("rise (\\d{4}) set (\\d{4}).*(\\d{2})h (\\d{2})m (\\d{2})s - (\\d)m (\\d{2})s (\\d{3})\", \"rise \\d{4} set \\d{4}.*\\d{2}h \\d{2}m \\d{2}s - \\dm \\d{2}s \\d{3}\", \"rise (\\d*) set (\\d*) .* (\\d*)h (\\d*)m (\\d*)s - (\\d*)m (\\d*)s (\\d*)\", \"rise \\d{4} set \\d{4} \\(:\\) \\d{2}h \\d{2}m \\d{2}s - \\dm \\d{2}s \\d{3}", opt);
    }
}