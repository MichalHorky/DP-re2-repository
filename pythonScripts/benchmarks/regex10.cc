#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:(\\+{1}|00)\\s{0,1}([0-9]{3}|[0-9]{2})\\s{0,1}\\-{0,1}\\s{0,1}([0-9]{2}|[1-9]{1})\\s{0,1}\\-{0,1}\\s{0,1}([0-9]{8}|[0-9]{7}))", opt);
    } else {
        RE2 pattern("(\\+{1}|00)\\s{0,1}([0-9]{3}|[0-9]{2})\\s{0,1}\\-{0,1}\\s{0,1}([0-9]{2}|[1-9]{1})\\s{0,1}\\-{0,1}\\s{0,1}([0-9]{8}|[0-9]{7})", opt);
    }
}