#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:\\w{3}-(\\d)-(\\d{14})-([\\w\\d]{8}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{12})-(\\w+)-([\\w.]+)-(\\w+-\\w+)-([\\w\\d]{8}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{12}-[\\w\\d]{4}))", opt);
    } else {
        RE2 pattern("\\w{3}-(\\d)-(\\d{14})-([\\w\\d]{8}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{12})-(\\w+)-([\\w.]+)-(\\w+-\\w+)-([\\w\\d]{8}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{4}-[\\w\\d]{12}-[\\w\\d]{4})", opt);
    }
}