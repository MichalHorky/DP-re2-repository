#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:{(\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}):.+\\s(\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}:.\\d{1,3})})", opt);
    } else {
        RE2 pattern("{(\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}):.+\\s(\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}:.\\d{1,3})}", opt);
    }
}