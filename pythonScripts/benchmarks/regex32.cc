#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:pattern=\"[A-Z]{2}\\d{2}\\s?\\d{4}\\s?\\d{4}\"\", \"pattern=\"[A-Z]{2}\\d{9}\"\", \"pattern=\"[A-Z]{2}[0-9]{9}\"\", \"pattern=\"[A-Z]{2}[000000000-999999999]{9}\"\", \"[000000000-999999999]\", \"pattern=\"[A-Z]{2}\\d{2} ?\\d{4} ?\\d{4}\")", opt);
    } else {
        RE2 pattern("pattern=\"[A-Z]{2}\\d{2}\\s?\\d{4}\\s?\\d{4}\"\", \"pattern=\"[A-Z]{2}\\d{9}\"\", \"pattern=\"[A-Z]{2}[0-9]{9}\"\", \"pattern=\"[A-Z]{2}[000000000-999999999]{9}\"\", \"[000000000-999999999]\", \"pattern=\"[A-Z]{2}\\d{2} ?\\d{4} ?\\d{4}\"", opt);
    }
}