#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:0\\d-\\d{4}-\\d{4}|0\\d{2}-\\d{3}-\\d{4}|0\\d{3}-\\d{2}-\\d{4}|0\\d{4}-\\d-\\d{4})", opt);
    } else {
        RE2 pattern("0\\d-\\d{4}-\\d{4}|0\\d{2}-\\d{3}-\\d{4}|0\\d{3}-\\d{2}-\\d{4}|0\\d{4}-\\d-\\d{4}", opt);
    }
}