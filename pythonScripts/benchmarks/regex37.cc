#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:\\s?\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d+Z|\\s?\\d{4}-\\d{2}-\\d{2}[_T]\\d{2}:\\d{2}:\\d{2}.\\d{5}|\\s?\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}|\\s?\\[\\d{2}\\/\\w{3}\\/\\d{4}\\s\\d{2}:\\d{2}:\\d{2}\\]?)", opt);
    } else {
        RE2 pattern("\\s?\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d+Z|\\s?\\d{4}-\\d{2}-\\d{2}[_T]\\d{2}:\\d{2}:\\d{2}.\\d{5}|\\s?\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}|\\s?\\[\\d{2}\\/\\w{3}\\/\\d{4}\\s\\d{2}:\\d{2}:\\d{2}\\]?", opt);
    }
}