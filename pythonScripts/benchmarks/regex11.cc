#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:\\D{0,2}[0]{0,3}[1]{0,1}\\D{0,2}([2-9])(\\d{2})\\D{0,2}(\\d{3})\\D{0,2}(\\d{3})\\D{0,2}(\\d{1})\\D{0,2})", opt);
    } else {
        RE2 pattern("\\D{0,2}[0]{0,3}[1]{0,1}\\D{0,2}([2-9])(\\d{2})\\D{0,2}(\\d{3})\\D{0,2}(\\d{3})\\D{0,2}(\\d{1})\\D{0,2}", opt);
    }
}