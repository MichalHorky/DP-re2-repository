#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:(429496729[0-6]|42949672[0-8]\\d|4294967[01]\\d{2}|429496[0-6]\\d{3}|42949[0-5]\\d{4}|4294[0-8]\\d{5}|429[0-3]\\d{6}|42[0-8]\\d{7}|4[01]\\d{8}|[1-3]\\d{9}|[1-9]\\d{8}|[1-9]\\d{7}|[1-9]\\d{6}|[1-9]\\d{5}|[1-9]\\d{4}|[1-9]\\d{3}|[1-9]\\d{2}|[1-9]\\d|\\d))", opt);
    } else {
        RE2 pattern("(429496729[0-6]|42949672[0-8]\\d|4294967[01]\\d{2}|429496[0-6]\\d{3}|42949[0-5]\\d{4}|4294[0-8]\\d{5}|429[0-3]\\d{6}|42[0-8]\\d{7}|4[01]\\d{8}|[1-3]\\d{9}|[1-9]\\d{8}|[1-9]\\d{7}|[1-9]\\d{6}|[1-9]\\d{5}|[1-9]\\d{4}|[1-9]\\d{3}|[1-9]\\d{2}|[1-9]\\d|\\d)", opt);
    }
}