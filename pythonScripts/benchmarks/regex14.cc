#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:^(.{1}:.{1}|.{2}:.{2}|.{3}:.{3}|.{4}:.{4}|.{5}:.{5}|.{6}:.{6}|.{7}:.{7})$)", opt);
    } else {
        RE2 pattern("^(.{1}:.{1}|.{2}:.{2}|.{3}:.{3}|.{4}:.{4}|.{5}:.{5}|.{6}:.{6}|.{7}:.{7})$", opt);
    }
}