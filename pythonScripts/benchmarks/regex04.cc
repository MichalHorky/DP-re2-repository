#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:(02\\d\\s?\\d{4}\\s?\\d{4})|((01|05)\\d{2}\\s?\\d{3}\\s?\\d{4})|((01|05)\\d{3}\\s?\\d{5,6})|((01|05)\\d{4}\\s?\\d{4,5}))", opt);
    } else {
        RE2 pattern("(02\\d\\s?\\d{4}\\s?\\d{4})|((01|05)\\d{2}\\s?\\d{3}\\s?\\d{4})|((01|05)\\d{3}\\s?\\d{5,6})|((01|05)\\d{4}\\s?\\d{4,5})", opt);
    }
}