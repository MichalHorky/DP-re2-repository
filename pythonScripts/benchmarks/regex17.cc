#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:^\\d{6}(\\d?0[^0]{1,2}0|\\d?1[^1]{1,2}1|\\d?2[^2]{1,2}2|\\d?3[^3]{1,2}3|\\d?4[^4]{1,2}4|\\d?5[^5]{1,2}5|\\d?6[^6]{1,2}6|\\d?7[^7]{1,2}7|\\d?8[^8]{1,2}8|\\d?9[^9]{1,2}9))", opt);
    } else {
        RE2 pattern("^\\d{6}(\\d?0[^0]{1,2}0|\\d?1[^1]{1,2}1|\\d?2[^2]{1,2}2|\\d?3[^3]{1,2}3|\\d?4[^4]{1,2}4|\\d?5[^5]{1,2}5|\\d?6[^6]{1,2}6|\\d?7[^7]{1,2}7|\\d?8[^8]{1,2}8|\\d?9[^9]{1,2}9)", opt);
    }
}