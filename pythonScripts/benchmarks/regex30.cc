#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:103.18.138.27:27015\", \"gost.gflclan.com:27015\", \"([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}:[0-9]{5})|([a-z].[a-z].[a-z]:[1-9]{5}))", opt);
    } else {
        RE2 pattern("103.18.138.27:27015\", \"gost.gflclan.com:27015\", \"([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}:[0-9]{5})|([a-z].[a-z].[a-z]:[1-9]{5})", opt);
    }
}