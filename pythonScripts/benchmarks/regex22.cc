#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:GR\\d{2}\\d{4}\\d{3}\\w\\w{4}\\w{4}\\w{4}\\w{3}\", \"GR\\d{2}-\\d{4}-\\d{3}\\w-\\w{4}-\\w{4}-\\w{4}-\\w{3}\", \"GR\\d{2} \\d{4} \\d{3}\\w \\w{4} \\w{4} \\w{4} \\w{3})", opt);
    } else {
        RE2 pattern("GR\\d{2}\\d{4}\\d{3}\\w\\w{4}\\w{4}\\w{4}\\w{3}\", \"GR\\d{2}-\\d{4}-\\d{3}\\w-\\w{4}-\\w{4}-\\w{4}-\\w{3}\", \"GR\\d{2} \\d{4} \\d{3}\\w \\w{4} \\w{4} \\w{4} \\w{3}", opt);
    }
}