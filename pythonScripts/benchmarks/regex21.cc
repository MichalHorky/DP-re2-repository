#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:time_pattern=r'\\*.{2}(\\d{4}) (\\d{2}) (\\d{2}) (\\d{2}) (\\d{2}) (\\d{2}\\.\\d{8})'\", \"*A 2014 12 31 23 59 59.123456\", \"np.str)", opt);
    } else {
        RE2 pattern("time_pattern=r'\\*.{2}(\\d{4}) (\\d{2}) (\\d{2}) (\\d{2}) (\\d{2}) (\\d{2}\\.\\d{8})'\", \"*A 2014 12 31 23 59 59.123456\", \"np.str", opt);
    }
}