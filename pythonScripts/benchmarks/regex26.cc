#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:I.+(Fab|Opt).+(\\d{4}\\/\\d{2}\\/\\d{2}).+(\\d{2}:\\d{2}:\\d{2}:\\d{3}).+Bus\\s\\/\\s(\\w+)\\s:\\W+\\n500.+003(\\d{7}).+\", \"\n)", opt);
    } else {
        RE2 pattern("I.+(Fab|Opt).+(\\d{4}\\/\\d{2}\\/\\d{2}).+(\\d{2}:\\d{2}:\\d{2}:\\d{3}).+Bus\\s\\/\\s(\\w+)\\s:\\W+\\n500.+003(\\d{7}).+\", \"\n", opt);
    }
}