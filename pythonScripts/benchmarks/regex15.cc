#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:^([0-8]\\d{8})|(\\d[0-8]\\d{7})|(\\d{2}[0-8]\\d{6})|(\\d{3}[0-8]\\d{5})|(\\d{4}[0-8]\\d{4})|(\\d{5}[0-8]\\d{3})|(\\d{6}[0-8]\\d{2})|(\\d{7}[0-8]\\d{1})|(\\d{8}[0-8])$)", opt);
    } else {
        RE2 pattern("^([0-8]\\d{8})|(\\d[0-8]\\d{7})|(\\d{2}[0-8]\\d{6})|(\\d{3}[0-8]\\d{5})|(\\d{4}[0-8]\\d{4})|(\\d{5}[0-8]\\d{3})|(\\d{6}[0-8]\\d{2})|(\\d{7}[0-8]\\d{1})|(\\d{8}[0-8])$", opt);
    }
}