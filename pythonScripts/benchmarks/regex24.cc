#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:10.22.39.106 10.242.29.27\", \"Aug 27 18:41:44 Testlab nixc[27354]: 207416484 {10.20.21.106:52907 10.20.21.27:80} http traffic\", \"grep -E -o '[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}.*[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}' log)", opt);
    } else {
        RE2 pattern("10.22.39.106 10.242.29.27\", \"Aug 27 18:41:44 Testlab nixc[27354]: 207416484 {10.20.21.106:52907 10.20.21.27:80} http traffic\", \"grep -E -o '[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}.*[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}' log", opt);
    }
}