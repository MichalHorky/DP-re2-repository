#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:\\w{15}\x01\\d{12}\x01\\d{2}.{6}(13[0-5]\\d|1400)[0-5]\\d801.*\", \"\\w{15}\x01\\d{12}\x01\\d{2}.{6}(13[0-5]\\d|14([0-2]\\d|30))[0-5]\\d801.*)", opt);
    } else {
        RE2 pattern("\\w{15}\x01\\d{12}\x01\\d{2}.{6}(13[0-5]\\d|1400)[0-5]\\d801.*\", \"\\w{15}\x01\\d{12}\x01\\d{2}.{6}(13[0-5]\\d|14([0-2]\\d|30))[0-5]\\d801.*", opt);
    }
}