#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:^([^0900000000-009499999]|[^0900000000-094000000]|\\d{9}|\\d{1}|\\d{2}|\\d{3}|\\d{4}|\\d{5}|\\d{6}|\\d{7}|\\d{8})$)", opt);
    } else {
        RE2 pattern("^([^0900000000-009499999]|[^0900000000-094000000]|\\d{9}|\\d{1}|\\d{2}|\\d{3}|\\d{4}|\\d{5}|\\d{6}|\\d{7}|\\d{8})$", opt);
    }
}