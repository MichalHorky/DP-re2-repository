#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:^QS([NDR])(.{4})(.{6})(\\d{8})(\\d{8}| {8})([01]{7})([ SH])([ ABXG])(.{4})(.{6})(.{8})(.{8})(.)$)", opt);
    } else {
        RE2 pattern("^QS([NDR])(.{4})(.{6})(\\d{8})(\\d{8}| {8})([01]{7})([ SH])([ ABXG])(.{4})(.{6})(.{8})(.{8})(.)$", opt);
    }
}