#include <re2/re2.h>
#include <iostream>

int main() {
    RE2::Options opt;
    opt.set_use_CsAs(true);
    //opt.set_unanchored(true);
    bool useSingleline = true;
    if (useSingleline) {
        RE2 pattern("(?s:(\\d|\\d{1,9}|1\\d{1,9}|20\\d{8}|213\\d{7}|2146\\d{6}|21473\\d{5}|214747\\d{4}|2147482\\d{3}|21474835\\d{2}|214748364[0-7]))", opt);
    } else {
        RE2 pattern("(\\d|\\d{1,9}|1\\d{1,9}|20\\d{8}|213\\d{7}|2146\\d{6}|21473\\d{5}|214747\\d{4}|2147482\\d{3}|21474835\\d{2}|214748364[0-7])", opt);
    }
}