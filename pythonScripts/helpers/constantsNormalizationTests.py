'''
Author: Michal Horky (xhorky23)
These outputs are for testing the normalization of regex and rewriting plus operator to star operator
'''

# Partial lists are only local
# Patterns for the source file
list_of_patterns = ['RE2 pattern1(\"a\", opt);\n', 'RE2 pattern2(\"abc\", opt);\n', 'RE2 pattern3(\"abcdefghijkl\", opt);\n', 
                    'RE2 pattern4(\"(a|bb|c)df\", opt);\n', 'RE2 pattern5(\"aba\", opt);\n', 'RE2 pattern6(\"a(a|bb|c)df\", opt);\n',
                    'RE2 pattern7(\"(a|bb|c)(d|ee|f)\", opt);\n', 'RE2 pattern8(\"(a|bb|c)a(d|ee|f)\", opt);\n',
                    'RE2 pattern9(\"(d|ee|f)\", opt);\n', 'RE2 pattern10(\"a{1,3}\", opt);\n', 'RE2 pattern11(\"(a{1,3})b{2,3}\", opt);\n',
                    'RE2 pattern12(\"(){20,30}\", opt);\n', 'RE2 pattern13(\"(a*){20,30}\", opt);\n', 
                    'RE2 pattern14(\"(a*b*c*){20,30}\", opt);\n', 'RE2 pattern15(\"(abcde){1,2}\", opt);\n',
                    'RE2 pattern16(\"(ac*){10,20}\", opt);\n', 'RE2 pattern17(\"a*b{1,3}\", opt);\n', 
                    'RE2 pattern18(\"aa*b{1,3}\", opt);\n', 'RE2 pattern19(\"(()a*){20,30}\", opt);\n',
                    'RE2 pattern20(\"(()a*()){20,30}\", opt);\n', 'RE2 pattern21(\"(a*()a*){20,30}\", opt);\n',
                    'RE2 pattern22(\"(()()()()){20,30}\", opt);\n', 'RE2 pattern23(\"(a*()a*()a*()a*a*){20,30}\", opt);\n',
                    'RE2 pattern24(\"(b*c*()()()d*()f*r*q*()a*){20,30}\", opt);\n', 'RE2 pattern25(\"((a*|b*)){20,30}\", opt);\n',
                    'RE2 pattern26(\"((a*|b*|c*)){20,30}\", opt);\n', 'RE2 pattern27(\"((a*|b*)c*){20,30}\", opt);\n',
                    'RE2 pattern28(\"((a*|())){20,30}\", opt);\n', 'RE2 pattern29(\"((()|())){20,30}\", opt);\n',
                    'RE2 pattern30(\"((a*|b*|c*|d*|e*|f*)){20,30}\", opt);\n', 'RE2 pattern31(\"(a*(a*|b*|()|q*)()c*){20,30}\", opt);\n',
                    'RE2 pattern32(\"(a*b*c*(a*|b*|o*)){20,30}\", opt);\n', 'RE2 pattern33(\"((()|()|()|()|())){20,30}\", opt);\n',
                    'RE2 pattern34(\"((()|b*|c*|())a*b*c*){20,30}\", opt);\n', 'RE2 pattern35(\"(((((a*|b*|c*))))){20,30}\", opt);\n',
                    'RE2 pattern36(\"((((((((a*|b*)a*)b*)c*)d*)))){20,30}\", opt);\n']


# Expected outputs
list_of_outputs = ['a\n', 'abc\n', 'abcdefghijkl\n', '(?:a|bb|c)df\n', 'aba\n', 'a(?:a|bb|c)df\n', 
                   '(?:a|bb|c)(?:d|ee|f)\n', '(?:a|bb|c)a(?:d|ee|f)\n', 'd|ee|f\n', 'a{1,3}\n', 'a{1,3}b{2,3}\n',
                   '(?:){0,30}\n', '(?:a*){0,30}\n', '(?:a*b*c*){0,30}\n', '(?:abcde){1,2}\n', '(?:ac*){10,20}\n',
                   'a*b{1,3}\n', 'aa*b{1,3}\n', '(?:(?:)a*){0,30}\n', '(?:(?:)a*(?:)){0,30}\n', '(?:a*(?:)a*){0,30}\n',
                   '(?:(?:)(?:)(?:)(?:)){0,30}\n', '(?:a*(?:)a*(?:)a*(?:)a*a*){0,30}\n','(?:b*c*(?:)(?:)(?:)d*(?:)f*r*q*(?:)a*){0,30}\n',
                   '(?:a*|b*){0,30}', '(?:a*|b*|c*){0,30}', '(?:(?:a*|b*)c*){0,30}', '(?:a*|(?:)){0,30}', '(?:(?:)|(?:)){0,30}',
                   '(?:a*|b*|c*|d*|e*|f*){0,30}', '(?:a*(?:a*|b*|(?:)|q*)(?:)c*){0,30}', '(?:a*b*c*(?:a*|b*|o*)){0,30}',
                   '(?:(?:)|(?:)|(?:)|(?:)|(?:)){0,30}', '(?:(?:(?:)|b*|c*|(?:))a*b*c*){0,30}', '(?:a*|b*|c*){0,30}',
                   '(?:(?:a*|b*)a*b*c*d*){0,30}']

# This variables will be used in other files
COMPILATION_FLAG = 'NORMALIZATIONTESTS'
START_OF_SOURCE_FILE = '#include <re2/re2.h>\n#include <iostream>\nint main() {\nRE2::Options opt;\nopt.set_use_CsAs(true);\n'
END_OF_SOURCE_FILE = '}'
DELIMITER = ';,;,;;,,;;,,'
TEST_NAMES = ['Normalization']
PRINT_DELIMITER_COMMAND = f'std::cout << \"{DELIMITER}\" << std::endl;\n'
LIST_OF_ALL_PATTERNS = [list_of_patterns]
LIST_OF_ALL_OUTPUTS = [list_of_outputs]