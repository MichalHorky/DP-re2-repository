'''
Author: Michal Horky (xhorky23)
These outputs are for testing the final states of counting automaton
For testing purposes, the output is printed by printFinalStates function (see derivatives.cc)
'''


# Partial lists are only local

# Pattern for the source file
list_of_patterns = ['RE2 pattern1(\"a{1,3}\", opt);\n', 'RE2 pattern2(\"b{200,300}\", opt);\n', 
                    'RE2 pattern3(\"ab{200,300}\", opt);\n', 'RE2 pattern4(\"(ab){200,300}\", opt);\n',
                    'RE2 pattern5(\"a(ab){200,300}\", opt);\n', 'RE2 pattern6(\"a{1,3}c\", opt);\n',
                    'RE2 pattern7(\"b{200,300}c\", opt);\n', 'RE2 pattern8(\"ab{200,300}c\", opt);\n',
                    'RE2 pattern9(\"(ab){200,300}c\", opt);\n', 'RE2 pattern10(\"a(ab){200,300}c\", opt);\n',
                    'RE2 pattern11(\"a(ab){200,300}qr\", opt);\n', 'RE2 pattern12(\"a{1,3}a{1,3}ab\", opt);\n',
                    'RE2 pattern13(\"a{0,3}a{0,3}ab\", opt);\n', 'RE2 pattern14(\".*a{1,3}a{1,3}a\", opt);\n',
                    'RE2 pattern15(\".*a{0,3}a{0,3}a\", opt);\n', 'RE2 pattern16(\"a{1,3}a{5,9}ab\", opt);\n',
                    'RE2 pattern17(\"a{1,3}b{5,9}ab\", opt);\n', 'RE2 pattern18(\"a{1,3}b{0,9}ab\", opt);\n',
                    'RE2 pattern19(\"a{0,3}b{0,9}ab\", opt);\n', 'RE2 pattern20(\"a{0,3}b{0,9}abb{0,9}\", opt);\n',
                    'RE2 pattern21(\"a{1,3}b{2,9}abb{2,9}\", opt);\n',
                    'RE2 pattern22(\"a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6}\", opt);\n',
                    'RE2 pattern23(\"(a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd\", opt);\n',
                    'RE2 pattern24(\"(a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})+abcd\", opt);\n',
                    'RE2 pattern25(\"(.{9})*\", opt);\n']

# Expected outputs
list_of_outputs = ['STATE: a{1,3}, Guard: CanExit, Counting loop: a{1,3}\nSTATE: , Guard: True',
                   'STATE: b{200,300}, Guard: CanExit, Counting loop: b{200,300}\nSTATE: , Guard: True',
                   'STATE: b{200,300}, Guard: CanExit, Counting loop: b{200,300}\nSTATE: , Guard: True',
                   'STATE: (?:ab){200,300}, Guard: CanExit, Counting loop: (?:ab){200,300}\nSTATE: , Guard: True',
                   'STATE: (ab){200,300}, Guard: CanExit, Counting loop: (ab){200,300}\nSTATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True',
                   'STATE: , Guard: True\nSTATE: b{0,9}, Guard: CanExit, Counting loop: b{0,9}1',
                   'STATE: , Guard: True\nSTATE: b{2,9}, Guard: CanExit, Counting loop: b{2,9}1',
                   'STATE: , Guard: True\nSTATE: b{2,9}c{0,5}d{0,6}, Guard: CanExit, Counting loop: b{2,9}1\n'
                    'STATE: c{0,5}d{0,6}, Guard: CanExit, Counting loop: c{0,5}\n'
                    'STATE: d{0,6}, Guard: CanExit, Counting loop: d{0,6}',
                   'STATE: , Guard: True\nSTATE: b{2,9}c{0,5}d{0,6}, Guard: CanExit, Counting loop: b{2,9}1\n'
                    'STATE: c{0,5}d{0,6}, Guard: CanExit, Counting loop: c{0,5}\n'
                    'STATE: d{0,6}, Guard: CanExit, Counting loop: d{0,6}',
                   'STATE: , Guard: True\nSTATE: d{1,6}, Guard: CanExit, Counting loop: d{1,6}',
                   'STATE: (?:[^\\n]{9})*, Guard: True\n'
                    'STATE: [^\\n]{9}, Guard: CanExit, Counting loop: [^\\n]{9}\nSTATE: , Guard: True\n'
                    'STATE: [^\\n]{9}(?:[^\\n]{9})*, Guard: CanExit, Counting loop: [^\\n]{9}'
                  ]


# This variables will be used in other files
COMPILATION_FLAG = 'CAFINALSTATESTESTS'
START_OF_SOURCE_FILE = '#include <re2/re2.h>\n#include <iostream>\nint main() {\nRE2::Options opt;\nopt.set_use_CsAs(true);\n'
END_OF_SOURCE_FILE = '}'
DELIMITER = ';,;,;;,,;;,,'
TEST_NAMES = ['CA final states']
PRINT_DELIMITER_COMMAND = f'std::cout << \"{DELIMITER}\" << std::endl;\n'
LIST_OF_ALL_PATTERNS = [list_of_patterns]
LIST_OF_ALL_OUTPUTS = [list_of_outputs]
