'''
Author: Michal Horky (xhorky23)
These outputs are for testing the ocorrect detection of equation and operand for derivatives computation
For testing purposes, the output is printed by printEquationTypeAndOperands function (see derivatives.cc)
'''

first_line = 'Concatenation equation, operands: \n'

# Partial lists are only local

# Patterns for the source file
list_of_patterns_literal = ['RE2 pattern1(\"a\", opt);\n', 'RE2 pattern2(\"b\", opt);\n', 'RE2 pattern3(\"c\", opt);\n', 'RE2 pattern4(\"d\", opt);\n']

# Expected outputs
list_of_outputs_literal = [first_line + '\ta\n\tepsilon\n',
                            first_line + '\tb\n\tepsilon\n',
                            first_line + '\tc\n\tepsilon\n',
                            first_line + '\td\n\tepsilon\n'
                            ]

# Patterns for the source file
list_of_patterns_literal_string = ['RE2 pattern1(\"ab\", opt);\n', 'RE2 pattern2(\"abc\", opt);\n', 'RE2 pattern3(\"abcd\", opt);\n', 
                                    'RE2 pattern4(\"abcedfghijklmnopqrstuvwxyz\", opt);\n']

# Expected outputs
list_of_outputs_literal_string = [first_line + '\ta\n\tb\n',
                                first_line + '\ta\n\tbc\n',
                                first_line + '\ta\n\tbcd\n',
                                first_line + '\ta\n\tbcedfghijklmnopqrstuvwxyz\n'
                                ]

# Patterns for the source file
list_of_patterns_concat = ['RE2 pattern1(\"aa{1,3}b\", opt);\n', 'RE2 pattern2(\"[b-r][a-c]d\", opt);\n', 'RE2 pattern3(\"b+a*c\", opt);\n', 
                            'RE2 pattern4(\"qa+c\", opt);\n', 'RE2 pattern5(\"[p-r]a?c\", opt);\n', 'RE2 pattern6(\"abcdc{1,4}qrm\", opt);\n', 
                            'RE2 pattern7(\"abcedf.*a{1,2}.*rps\", opt);\n', 'RE2 pattern8(\"pa{1,4}a{1,3}a{1,5}a{1,6}a{1,9}a{1,2}\", opt);\n', 
                            'RE2 pattern9(\"pes(def|qed|der|dum|poil)a{1,3}qer\", opt);\n', 'RE2 pattern10(\"bl[^\\\\w][\\\\w][p-q]ab\", opt);\n', 
                            'RE2 pattern11(\"((k[a-c]da{1,3}))a{2,5}b\", opt);\n', 'RE2 pattern12(\"q(a{1,3}abcedfg)([d-q]a{1,4})abcde", opt);\n',
                            'RE2 pattern13(\"(a)(b)(c)(d)\", opt);\n', 'RE2 pattern14(\"re2[^a][^b][^c][^d][^e]", opt);\n', 
                            'RE2 pattern15(\"AB[^C]DE\", opt);', 'RE2 pattern16(\"a[a-z]b\", opt);', 
                            'RE2 pattern17(\"((krk[a-c]da{1,3}))a{2,5}b\", opt);', 'RE2 pattern18(\"(((aa{1,3}b)))\", opt);\n', 
                            'RE2 pattern19(\"(([b-r][a-c]d))\", opt);\n', 'RE2 pattern20(\"(((b+a*c)))\", opt);\n', 
                            'RE2 pattern21(\"((((qa+c))))\", opt);\n', 'RE2 pattern22(\"(([p-r]a?c))\", opt);\n', 
                            'RE2 pattern23(\"((abcdc{1,4}qrm))\", opt);\n', 'RE2 pattern24(\"(((abcedf.*a{1,2}.*rps)))\", opt);\n', 
                            'RE2 pattern25(\"((pa{1,4}a{1,3}a{1,5}a{1,6}a{1,9}a{1,2}))\", opt);\n', 
                            'RE2 pattern26(\"((pes(def|qed|der|dum|poil)a{1,3}qer))\", opt);\n', 
                            'RE2 pattern27(\"((bl[^\\\\w][\\\\w][p-q]ab))\", opt);\n',
                            'RE2 pattern28(\"(((AB[^C]DE)))\", opt);', 'RE2 pattern29(\"((a[a-z]b))\", opt);',
                            # These three will be Alternation because a? is rewritten to eps|a, but in re2 it is concat
                            'RE2 pattern30(\"a?b\", opt);', 'RE2 pattern31(\"(a?b)\", opt);', 'RE2 pattern32(\"((a?b))\", opt);',
                            'RE2 pattern33(\"(k[a-d][e-r]a?)\", opt);', 'RE2 pattern34(\"(((a?bce[e-q]a{1,3})))\", opt);', 
                            'RE2 pattern35(\"(((((abca?)))))\", opt);', 'RE2 pattern36(\"(([a-q]a{1,3}b))a{2,5}b\", opt);\n']

# Expected outputs
# . is rewritten to [^n], \w to 0-9A-Z_a-z
# der|dum is rewritten to d(?:er|um) but that one will not change operands
# epsilon (kRegexpEmptyMatch) is rewritten to (?:)
list_of_outputs_concat = [first_line +'\ta\n\ta{1,3}b\n',
                        first_line +'\t[b-r]\n\t[a-c]d\n',
                        first_line +'\tb\n\tb*a*c\n',
                        first_line +'\tq\n\taa*c\n',
                        first_line +'\t[p-r]\n\t(?:(?:)|a)c\n',
                        first_line +'\ta\n\tbcdc{1,4}qrm\n',
                        first_line +'\ta\n\tbcedf[^\\n]*a{1,2}[^\\n]*rps\n',
                        first_line +'\tp\n\ta{1,4}a{1,3}a{1,5}a{1,6}a{1,9}a{1,2}\n',
                        first_line +'\tp\n\tes(?:def|qed|d(?:er|um)|poil)a{1,3}qer\n',
                        first_line +'\tb\n\tl[^0-9A-Z_a-z][0-9A-Z_a-z][p-q]ab\n',
                        first_line +'\tk\n\t[a-c]da{1,3}a{2,5}b\n',
                        first_line +'\tq\n\ta{1,3}abcedfg[d-q]a{1,4}abcde\n',
                        first_line +'\ta\n\tbcd\n',
                        first_line +'\tr\n\te2[^a][^b][^c][^d][^e]\n',
                        first_line +'\tA\n\tB[^C]DE\n',
                        first_line +'\ta\n\t[a-z]b\n',
                        first_line +'\tk\n\trk[a-c]da{1,3}a{2,5}b\n',
                        first_line +'\ta\n\ta{1,3}b\n',
                        first_line +'\t[b-r]\n\t[a-c]d\n',
                        first_line +'\tb\n\tb*a*c\n',
                        first_line +'\tq\n\taa*c\n',
                        first_line +'\t[p-r]\n\t(?:(?:)|a)c\n',
                        first_line +'\ta\n\tbcdc{1,4}qrm\n',
                        first_line +'\ta\n\tbcedf[^\\n]*a{1,2}[^\\n]*rps\n',
                        first_line +'\tp\n\ta{1,4}a{1,3}a{1,5}a{1,6}a{1,9}a{1,2}\n',
                        first_line +'\tp\n\tes(?:def|qed|d(?:er|um)|poil)a{1,3}qer\n',
                        first_line +'\tb\n\tl[^0-9A-Z_a-z][0-9A-Z_a-z][p-q]ab\n',
                        first_line +'\tA\n\tB[^C]DE\n',
                        first_line +'\ta\n\t[a-z]b\n',
                        'Alternation equation, operands: \n\t(?:)|a\n\tb\n',
                        'Alternation equation, operands: \n\t(?:)|a\n\tb\n',
                        'Alternation equation, operands: \n\t(?:)|a\n\tb\n',
                        first_line +'\tk\n\t[a-d][e-r](?:(?:)|a)\n',
                        'Alternation equation, operands: \n\t(?:)|a\n\tbce[e-q]a{1,3}\n',
                        first_line +'\ta\n\tbc(?:(?:)|a)\n',
                        first_line +'\t[a-q]\n\ta{1,3}ba{2,5}b\n',
                        ]

first_line = 'Epsilon equation\n'

# Patterns for the source file
list_of_patterns_empty = ['RE2 pattern1(\"()\", opt);\n', 'RE2 pattern2(\"(())\", opt);\n', 
                        'RE2 pattern3(\"((()))\", opt);\n', 'RE2 pattern4(\"()e\", opt);\n']

# Expected outputs
list_of_outputs_empty = [first_line + '\tepsilon\n\tepsilon\n',
                        first_line + '\tepsilon\n\tepsilon\n',
                        first_line + '\tepsilon\n\tepsilon\n',
                        'Concatenation equation, operands: \n\te\n\tepsilon\n']

first_line = 'Repetition equation, operands: \n'

# Patterns for the source file
list_of_patterns_repetition = ['RE2 pattern1(\"a*b\", opt);\n', 'RE2 pattern2(\"a*abcdefgh\", opt);\n', 'RE2 pattern3(\"(((((a*b)))))\", opt);\n',
                            'RE2 pattern4(\"(((((a*abcdefgh)))))\", opt);\n', 'RE2 pattern5(\"a*(abcdef)\", opt);\n', 
                            'RE2 pattern6(\"a*[a-c]\", opt);\n', 'RE2 pattern7(\"a*a*a*a*a*bcde\", opt);\n', 'RE2 pattern8(\"a*a?\", opt);\n',
                            'RE2 pattern9(\"a*a+\", opt);\n', 'RE2 pattern10(\"a*()\", opt);\n', 'RE2 pattern11(\"a*(abcd(efd+p?[a-c]))\", opt);\n',
                            'RE2 pattern12(\"a*(a|b)\", opt);\n', 'RE2 pattern13(\"a*((((([a-c])))))\", opt);\n', 
                            'RE2 pattern14(\"a*q?c?e?q?(abcdef)\", opt);\n', 'RE2 pattern15(\"((((a*))))\", opt);\n', 
                            'RE2 pattern16(\"a*\", opt);\n', 'RE2 pattern17(\"a*[abcedfghijklmno]\", opt);\n', 
                            'RE2 pattern18(\"(a*abce)a*b\", opt);\n']

# Expected outputs
# (a|b) is rewritten to ([a-b]), [abcedfghijklmno] to [a-o]
list_of_outputs_repetition = [first_line +'\ta*\n\tb\n',
                            first_line +'\ta*\n\tabcdefgh\n',
                            first_line +'\ta*\n\tb\n',
                            first_line +'\ta*\n\tabcdefgh\n',
                            first_line +'\ta*\n\tabcdef\n',
                            first_line +'\ta*\n\t[a-c]\n',
                            first_line +'\ta*\n\ta*a*a*a*bcde\n',
                            first_line +'\ta*\n\t(?:)|a\n', 
                            first_line +'\ta*\n\taa*\n',
                            first_line +'\ta*\n\tepsilon\n',
                            first_line +'\ta*\n\tabcdefdd*(?:(?:)|p)[a-c]\n',
                            first_line +'\ta*\n\t[a-b]\n',
                            first_line +'\ta*\n\t[a-c]\n',
                            first_line +'\ta*\n\t(?:(?:)|q)(?:(?:)|c)(?:(?:)|e)(?:(?:)|q)abcdef\n',
                            first_line +'\ta*\n\tepsilon\n',
                            first_line +'\ta*\n\tepsilon\n',
                            first_line +'\ta*\n\t[a-o]\n',
                            first_line +'\ta*\n\tabcea*b\n']

first_line = 'Concatenation equation, operands: \n'

# Patterns for the source file
list_of_patterns_plus_operator = ['RE2 pattern1(\"a+b\", opt);\n', 'RE2 pattern2(\"a+abcdefgh\", opt);\n', 'RE2 pattern3(\"(((((a+b)))))\", opt);\n',
                            'RE2 pattern4(\"(((((a+abcdefgh)))))\", opt);\n', 'RE2 pattern5(\"a+(abcdef)\", opt);\n', 
                            'RE2 pattern6(\"a+[a-c]\", opt);\n', 'RE2 pattern7(\"a+a*a*a*a*bcde\", opt);\n', 'RE2 pattern8(\"a+a?\", opt);\n',
                            'RE2 pattern9(\"a+a+\", opt);\n', 'RE2 pattern10(\"a+()\", opt);\n', 'RE2 pattern11(\"a+(abcd(efd+p?[a-c]))\", opt);\n',
                            'RE2 pattern12(\"a+(a|b)\", opt);\n', 'RE2 pattern13(\"a+((((([a-c])))))\", opt);\n', 
                            'RE2 pattern14(\"(a+abce)a*b\", opt);\n', 'RE2 pattern15(\"((((a+))))\", opt);\n', 
                            'RE2 pattern16(\"a+\", opt);\n', 'RE2 pattern17(\"a+[abcedfghijklmno]\", opt);\n', 
                            'RE2 pattern18(\"a+b+c+d+e+[a-c]a*\", opt);\n', 'RE2 pattern19(\"^abcd\", opt);\n',
                            'RE2 pattern20(\"abcd$\", opt);\n', 'RE2 pattern21(\"^abcd$\", opt);\n',
                            'RE2 pattern22(\".abc\", opt);\n', 'RE2 pattern23(\"\\\\Ca*bd\", opt);\n',
                            'RE2 pattern24(\"\\\\bb*cde\", opt);\n', 'RE2 pattern25(\"\\\\Bb*cde\", opt);\n',
                            'RE2 pattern26(\"(?m:^b*cde)\", opt);\n', 'RE2 pattern27(\"(?m:bc*de$)\", opt);\n',
                            'RE2 pattern28(\"abc\\\\bb*cde\", opt);\n', 'RE2 pattern29(\"abc\\\\Bb*cde\", opt);\n',
                            'RE2 pattern30(\"abc\\\\Cb*cde\", opt);\n']

# Expected outputs
# Plus operator will be rewritten in this style: a+ -> aa*, (abcd)+ -> abcd(abcd)* etc.
# So in the end, it will be concatenation
# (a|b) is rewritten to ([a-b]), [abcedfghijklmno] to [a-o]
list_of_outputs_plus_operator = [first_line +'\ta\n\ta*b\n',
                            first_line +'\ta\n\ta*abcdefgh\n',
                            first_line +'\ta\n\ta*b\n',
                            first_line +'\ta\n\ta*abcdefgh\n',
                            first_line +'\ta\n\ta*abcdef\n',
                            first_line +'\ta\n\ta*[a-c]\n',
                            first_line +'\ta\n\ta*a*a*a*a*bcde\n',
                            first_line +'\ta\n\ta*(?:(?:)|a)\n', 
                            first_line +'\ta\n\ta*aa*\n',
                            first_line +'\ta\n\ta*(?:)\n',
                            first_line +'\ta\n\ta*abcdefdd*(?:(?:)|p)[a-c]\n',
                            first_line +'\ta\n\ta*[a-b]\n',
                            first_line +'\ta\n\ta*[a-c]\n',
                            first_line +'\ta\n\ta*abcea*b\n',
                            first_line +'\ta\n\ta*\n',
                            first_line +'\ta\n\ta*\n',
                            first_line +'\ta\n\ta*[a-o]\n',
                            first_line +'\ta\n\ta*bb*cc*dd*ee*[a-c]a*\n',
                            first_line +'\ta\n\tbcd\n',
                            first_line +'\ta\n\tbcd\n',
                            first_line +'\ta\n\tbcd\n',
                            first_line +'\t[^\\n]\n\tabc\n',
                            first_line +'\t\\C\n\ta*bd\n',
                            first_line +'\t\\b\n\tb*cde\n',
                            first_line +'\t\\B\n\tb*cde\n',
                            first_line +'\t^\n\tb*cde\n',
                            first_line +'\tb\n\tc*de$\n',
                            first_line +'\ta\n\tbc\\bb*cde\n',
                            first_line +'\ta\n\tbc\\Bb*cde\n',
                            first_line +'\ta\n\tbc\\Cb*cde\n']

first_line = 'Alternation equation, operands: \n'

# Patterns for the source file
list_of_patterns_alternation = ['RE2 pattern1(\"a|qq\", opt);\n', 'RE2 pattern2(\"def|abc\", opt);\n', 'RE2 pattern3(\"a*|a*\", opt);\n',
                            'RE2 pattern4(\"[a-b]qq|[d-r]\", opt);\n', 'RE2 pattern5(\"a?|b?\", opt);\n', 
                            'RE2 pattern6(\"a+|b\", opt);\n', 'RE2 pattern7(\"j*|a\", opt);\n', 'RE2 pattern8(\"a*p*|b+c+\", opt);\n',
                            'RE2 pattern9(\"(a|qq)c\", opt);\n', 'RE2 pattern10(\"(a*|b*)c\", opt);\n', 'RE2 pattern11(\"(a?|b?)p*\", opt);\n',
                            'RE2 pattern12(\"([a-c]|qq)acd\", opt);\n', 'RE2 pattern13(\"(qq|b)\", opt);\n', 
                            'RE2 pattern14(\"(((a|cq)))\", opt);\n', 'RE2 pattern15(\"(a|())\", opt);\n', 
                            'RE2 pattern16(\"(a|qq)()\", opt);\n', 'RE2 pattern17(\"()|c\", opt);\n', 
                            'RE2 pattern18(\"()|()\", opt);\n']

# Expected outputs
list_of_outputs_alternation = [first_line +'\ta|qq\n\tepsilon\n',
                            first_line +'\tdef|abc\n\tepsilon\n',
                            first_line +'\ta*|a*\n\tepsilon\n',
                            first_line +'\t[a-b]qq|[d-r]\n\tepsilon\n',
                            first_line +'\t(?:)|a|(?:)|b\n\tepsilon\n',
                            first_line +'\taa*|b\n\tepsilon\n',
                            first_line +'\tj*|a\n\tepsilon\n',
                            first_line +'\ta*p*|bb*cc*\n\tepsilon\n', 
                            first_line +'\ta|qq\n\tc\n',
                            first_line +'\ta*|b*\n\tc\n',
                            first_line +'\t(?:)|a|(?:)|b\n\tp*\n',
                            first_line +'\t[a-c]|qq\n\tacd\n',
                            first_line +'\tqq|b\n\tepsilon\n',
                            first_line +'\ta|cq\n\tepsilon\n',
                            first_line +'\ta|(?:)\n\tepsilon\n',
                            first_line +'\ta|qq\n\tepsilon\n',
                            first_line +'\t(?:)|c\n\tepsilon\n',
                            first_line +'\t(?:)|(?:)\n\tepsilon\n']

first_line = 'Concatenation equation, operands: \n'

# Patterns for the source file
list_of_patterns_char_class = ['RE2 pattern1(\"[a-d]\", opt);\n', 'RE2 pattern2(\"[abcd]\", opt);\n', 'RE2 pattern3(\"[a-c]q[d-f]\", opt);\n',
                            'RE2 pattern4(\"[a-d]qqq\", opt);\n', 'RE2 pattern5(\"(([a-c]))\", opt);\n', 
                            'RE2 pattern6(\"[aqr]\", opt);\n', 'RE2 pattern7(\"[aqr]q*\", opt);\n', 'RE2 pattern8(\"[aq]r[dm]\", opt);\n',
                            'RE2 pattern9(\"[a-c]av*\", opt);\n', 'RE2 pattern10(\"[d-f]b+\", opt);\n', 'RE2 pattern11(\"[p-q][d-e]\", opt);\n',
                            'RE2 pattern12(\"([a-c])qqq\", opt);\n']

# Expected outputs
list_of_outputs_char_class = [first_line +'\t[a-d]\n\tepsilon\n',
                            first_line +'\t[a-d]\n\tepsilon\n',
                            first_line +'\t[a-c]\n\tq[d-f]\n',
                            first_line +'\t[a-d]\n\tqqq\n',
                            first_line +'\t[a-c]\n\tepsilon\n',
                            first_line +'\t[aq-r]\n\tepsilon\n',
                            first_line +'\t[aq-r]\n\tq*\n',
                            first_line +'\t[aq]\n\tr[dm]\n', 
                            first_line +'\t[a-c]\n\tav*\n',
                            first_line +'\t[d-f]\n\tbb*\n',
                            first_line +'\t[p-q]\n\t[d-e]\n',
                            first_line +'\t[a-c]\n\tqqq\n']



first_line = 'Counted repetition equation, operands: \n'

# Patterns for the source file
list_of_patterns_counted_repetition = ['RE2 pattern1(\"a{1,3}\", opt);\n', 'RE2 pattern2(\"a{1,3}[abcd]\", opt);\n', 
                                    'RE2 pattern3(\"a{1,6}b\", opt);\n', 'RE2 pattern4(\"a{1,3}b{1,3}def\", opt);\n', 
                                    'RE2 pattern5(\"((a{1,18}))ab{30,32}\", opt);\n', 'RE2 pattern6(\"(a{2,4}(b{2,6}))cde\", opt);\n', 
                                    'RE2 pattern7(\"a{3,4}(abced)\", opt);\n', 'RE2 pattern8(\"a{1,2}q+\", opt);\n', 
                                    'RE2 pattern9(\"p{3,4}a?\", opt);\n', 'RE2 pattern10(\"w{1,4}()\", opt);\n', 
                                    'RE2 pattern11(\"r{12,16}()bced\", opt);\n', 'RE2 pattern12(\"a{1,16}bce()qwer{1,3}[a-f]\", opt);\n']

# Expected outputs
list_of_outputs_counted_repetition = [first_line +'\ta{1,3}\n\tepsilon\n',
                                    first_line +'\ta{1,3}\n\t[a-d]\n',
                                    first_line +'\ta{1,6}\n\tb\n',
                                    first_line +'\ta{1,3}\n\tb{1,3}def\n',
                                    first_line +'\ta{1,18}\n\tab{30,32}\n',
                                    first_line +'\ta{2,4}\n\tb{2,6}cde\n',
                                    first_line +'\ta{3,4}\n\tabced\n',
                                    first_line +'\ta{1,2}\n\tqq*\n', 
                                    first_line +'\tp{3,4}\n\t(?:)|a\n',
                                    first_line +'\tw{1,4}\n\tepsilon\n',
                                    first_line +'\tr{12,16}\n\t(?:)bced\n',
                                    first_line +'\ta{1,16}\n\tbce(?:)qwer{1,3}[a-f]\n']



# This variables will be used in other files
COMPILATION_FLAG = 'EQUATIONTYPETESTS'
START_OF_SOURCE_FILE = '#include <re2/re2.h>\n#include <iostream>\nint main() {\nRE2::Options opt;\nopt.set_use_CsAs(true);\n'
END_OF_SOURCE_FILE = '}'
DELIMITER = ';,;,;;,,;;,,'
TEST_NAMES = ['Literal', 'Literal String', 'Concat', 'Empty regexp', 'Repetition', 'Plus operator', 'Alternation', 'Char class', 'Counted repetition',
                'Special (start/end line etc.)']
PRINT_DELIMITER_COMMAND = f'std::cout << \"{DELIMITER}\" << std::endl;\n'
LIST_OF_ALL_PATTERNS = [list_of_patterns_literal, list_of_patterns_literal_string, list_of_patterns_concat, list_of_patterns_empty,
                        list_of_patterns_repetition, list_of_patterns_plus_operator, list_of_patterns_alternation, list_of_patterns_char_class,
                        list_of_patterns_counted_repetition]
LIST_OF_ALL_OUTPUTS = [list_of_outputs_literal, list_of_outputs_literal_string, list_of_outputs_concat, list_of_outputs_empty,
                        list_of_outputs_repetition, list_of_outputs_plus_operator, list_of_outputs_alternation, list_of_outputs_char_class,
                        list_of_outputs_counted_repetition]