'''
Author: Michal Horky (xhorky23)
These outputs are for testing the computation of the scope
For testing purposes, the output is printed by printCaStates function (see derivatives.cc)
'''


# Partial lists are only local
# Patterns for the source file
list_of_no_scope_patterns = ['RE2 pattern1(\"a\", opt);\n', 'RE2 pattern2(\"abc\", opt);\n', 'RE2 pattern3(\"abcdefghijkl\", opt);\n', 
                             'RE2 pattern4(\"(a|bb|c)df\", opt);\n', 'RE2 pattern5(\"aba\", opt);\n', 'RE2 pattern6(\"a(a|bb|c)df\", opt);\n',
                             'RE2 pattern7(\"(a|bb|c)(d|ee|f)\", opt);\n', 'RE2 pattern8(\"(a|bb|c)a(d|ee|f)\", opt);\n',
                             'RE2 pattern9(\"(d|ee|f)\", opt);\n']

# Expected outputs
# Outputs will contain states that will not be in automata, but that are necessary to compute other states.
list_of_no_scope_outputs = ['State: , Scope: \nState: a, Scope: ',
                            'State: , Scope: \nState: abc, Scope: \nState: bc, Scope: \nState: c, Scope: ',
                            'State: , Scope: \nState: abcdefghijkl, Scope: \nState: bcdefghijkl, Scope: \n'
                              'State: cdefghijkl, Scope: \nState: defghijkl, Scope: \nState: efghijkl, Scope: \n'
                              'State: fghijkl, Scope: \nState: ghijkl, Scope: \nState: hijkl, Scope: \n'
                              'State: ijkl, Scope: \nState: jkl, Scope: \nState: kl, Scope: \nState: l, Scope: ',
                            'State: , Scope: \nState: (?:a|bb|c)df, Scope: \nState: adf, Scope: \nState: bbdf, Scope: \n'
                              'State: bdf, Scope: \nState: cdf, Scope: \nState: df, Scope: \nState: f, Scope: ',
                            'State: , Scope: \nState: a, Scope: \nState: aba, Scope: \nState: ba, Scope: ',
                            'State: , Scope: \nState: (?:a|bb|c)df, Scope: \nState: a(?:a|bb|c)df, Scope: \n'
                              'State: adf, Scope: \nState: bbdf, Scope: \nState: bdf, Scope: \nState: cdf, Scope: \n'
                              'State: df, Scope: \nState: f, Scope: ',
                            'State: , Scope: \nState: (?:a|bb|c)(?:d|ee|f), Scope: \nState: a(?:d|ee|f), Scope: \n'
                              'State: b(?:d|ee|f), Scope: \nState: bb(?:d|ee|f), Scope: \nState: c(?:d|ee|f), Scope: \n'
                              'State: d, Scope: \nState: d|ee|f, Scope: \nState: e, Scope: \nState: ee, Scope: \n'
                              'State: f, Scope: ',
                            'State: , Scope: \nState: (?:a|bb|c)a(?:d|ee|f), Scope: \nState: a(?:d|ee|f), Scope: \n'
                              'State: aa(?:d|ee|f), Scope: \nState: ba(?:d|ee|f), Scope: \n'
                              'State: bba(?:d|ee|f), Scope: \nState: ca(?:d|ee|f), Scope: \nState: d, Scope: \n'
                              'State: d|ee|f, Scope: \nState: e, Scope: \nState: ee, Scope: \nState: f, Scope: ',
                            'State: , Scope: \nState: d, Scope: \nState: d|ee|f, Scope: \nState: e, Scope: \n'
                              'State: ee, Scope: \nState: f, Scope: '
                            ]


list_of_patterns = ['RE2 pattern1(\"a{1,3}\", opt);\n', 'RE2 pattern2(\"b{200,300}\", opt);\n', 
                    'RE2 pattern3(\"ab{200,300}\", opt);\n', 'RE2 pattern4(\"(ab){200,300}\", opt);\n',
                    'RE2 pattern5(\"a(ab){200,300}\", opt);\n', 'RE2 pattern6(\"a{1,3}c\", opt);\n',
                    'RE2 pattern7(\"b{200,300}c\", opt);\n', 'RE2 pattern8(\"ab{200,300}c\", opt);\n',
                    'RE2 pattern9(\"(ab){200,300}c\", opt);\n', 'RE2 pattern10(\"a(ab){200,300}c\", opt);\n',
                    'RE2 pattern11(\"a(ab){200,300}qr\", opt);\n', 'RE2 pattern12(\"a{1,3}a{1,3}ab\", opt);\n',
                    'RE2 pattern13(\"a{0,3}a{0,3}ab\", opt);\n', 'RE2 pattern14(\".*a{1,3}a{1,3}a\", opt);\n',
                    'RE2 pattern15(\".*a{0,3}a{0,3}a\", opt);\n', 'RE2 pattern16(\"a{1,3}a{5,9}ab\", opt);\n',
                    'RE2 pattern17(\"a{1,3}b{5,9}ab\", opt);\n', 'RE2 pattern18(\"a{1,3}b{0,9}ab\", opt);\n',
                    'RE2 pattern19(\"a{0,3}b{0,9}ab\", opt);\n', 'RE2 pattern20(\"a{0,3}b{0,9}abb{0,9}\", opt);\n',
                    'RE2 pattern21(\"a{1,3}b{2,9}abb{2,9}\", opt);\n',
                    'RE2 pattern22(\"a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6}\", opt);\n',
                    'RE2 pattern23(\"(a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd\", opt);\n',
                    'RE2 pattern24(\"(a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})+abcd\", opt);\n',
                    'RE2 pattern25(\"(.{9})*\", opt);\n']


list_of_outputs = ['State: , Scope: \nState: a, Scope: \nState: a{1,3}, Scope: a{1,3}',
                   'State: , Scope: \nState: b, Scope: \nState: b{200,300}, Scope: b{200,300}',
                   'State: , Scope: \nState: ab{200,300}, Scope: \nState: b, Scope: \nState: b{200,300}, Scope: b{200,300}',
                   'State: , Scope: \nState: (?:ab){200,300}, Scope: (?:ab){200,300}\nState: ab, Scope: \nState: b, Scope: \n'
                    'State: b(?:ab){200,300}, Scope: (?:ab){200,300}',
                   'State: , Scope: \nState: (ab), Scope: \nState: (ab){200,300}, Scope: (ab){200,300}\n'
                    'State: a(ab){200,300}, Scope: \nState: b, Scope: \nState: b(ab){200,300}, Scope: (ab){200,300}',
                   'State: , Scope: \nState: a, Scope: \nState: a{1,3}c, Scope: a{1,3}\nState: c, Scope: ',
                   'State: , Scope: \nState: b, Scope: \nState: b{200,300}c, Scope: b{200,300}\nState: c, Scope: ',
                   'State: , Scope: \nState: ab{200,300}c, Scope: \nState: b, Scope: \n'
                    'State: b{200,300}c, Scope: b{200,300}\nState: c, Scope: ',
                   'State: , Scope: \nState: (ab), Scope: \nState: (ab){200,300}c, Scope: (ab){200,300}\n'
                    'State: b, Scope: \nState: b(ab){200,300}c, Scope: (ab){200,300}\nState: c, Scope: ',
                   'State: , Scope: \nState: (ab), Scope: \nState: (ab){200,300}c, Scope: (ab){200,300}\n'
                    'State: a(ab){200,300}c, Scope: \nState: b, Scope: \nState: b(ab){200,300}c, Scope: (ab){200,300}\n'
                    'State: c, Scope: ',
                   'State: , Scope: \nState: (ab), Scope: \nState: (ab){200,300}qr, Scope: (ab){200,300}\n'
                    'State: a(ab){200,300}qr, Scope: \nState: b, Scope: \nState: b(ab){200,300}qr, Scope: (ab){200,300}\n'
                    'State: qr, Scope: \nState: r, Scope: ',
                   'State: , Scope: \nState: a, Scope: \nState: ab, Scope: \nState: a{1,3}ab, Scope: a{1,3}1\n'
                    'State: a{1,3}a{1,3}ab, Scope: a{1,3}\nState: b, Scope: ',
                   'State: , Scope: \nState: a, Scope: \nState: ab, Scope: \nState: a{0,3}ab, Scope: a{0,3}1\n'
                    'State: a{0,3}a{0,3}ab, Scope: a{0,3}\nState: b, Scope: ',
                   'State: , Scope: \nState: [^\\n], Scope: \nState: [^\\n]*a{1,3}a{1,3}a, Scope: \n'
                    'State: a, Scope: \nState: a{1,3}a, Scope: a{1,3}1\nState: a{1,3}a{1,3}a, Scope: a{1,3}',
                   'State: , Scope: \nState: [^\\n], Scope: \nState: [^\\n]*a{0,3}a{0,3}a, Scope: \n'
                    'State: a, Scope: \nState: a{0,3}a, Scope: a{0,3}1\nState: a{0,3}a{0,3}a, Scope: a{0,3}',
                   'State: , Scope: \nState: a, Scope: \nState: ab, Scope: \nState: a{1,3}a{5,9}ab, Scope: a{1,3}\n'
                    'State: a{5,9}ab, Scope: a{5,9}\nState: b, Scope: ',
                   'State: , Scope: \nState: a, Scope: \nState: ab, Scope: \nState: a{1,3}b{5,9}ab, Scope: a{1,3}\n'
                    'State: b, Scope: \nState: b{5,9}ab, Scope: b{5,9}',
                   'State: , Scope: \nState: a, Scope: \nState: ab, Scope: \nState: a{1,3}b{0,9}ab, Scope: a{1,3}\n'
                    'State: b, Scope: \nState: b{0,9}ab, Scope: b{0,9}',
                   'State: , Scope: \nState: a, Scope: \nState: ab, Scope: \nState: a{0,3}b{0,9}ab, Scope: a{0,3}\n'
                    'State: b, Scope: \nState: b{0,9}ab, Scope: b{0,9}',
                   'State: , Scope: \nState: a, Scope: \nState: abb{0,9}, Scope: \n'
                    'State: a{0,3}b{0,9}abb{0,9}, Scope: a{0,3}\nState: b, Scope: \nState: bb{0,9}, Scope: \n'
                    'State: b{0,9}, Scope: b{0,9}1\nState: b{0,9}abb{0,9}, Scope: b{0,9}',
                   'State: , Scope: \nState: a, Scope: \nState: abb{2,9}, Scope: \n'
                    'State: a{1,3}b{2,9}abb{2,9}, Scope: a{1,3}\nState: b, Scope: \nState: bb{2,9}, Scope: \n'
                    'State: b{2,9}, Scope: b{2,9}1\nState: b{2,9}abb{2,9}, Scope: b{2,9}',
                   'State: , Scope: \nState: a, Scope: \nState: abb{2,9}c{0,5}d{0,6}, Scope: \n'
                    'State: a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6}, Scope: a{1,3}\nState: b, Scope: \n'
                    'State: bb{2,9}c{0,5}d{0,6}, Scope: \nState: b{2,9}abb{2,9}c{0,5}d{0,6}, Scope: b{2,9}\n'
                    'State: b{2,9}c{0,5}d{0,6}, Scope: b{2,9}1\nState: c, Scope: \nState: c{0,5}d{0,6}, Scope: c{0,5}\n'
                    'State: d, Scope: \nState: d{0,6}, Scope: d{0,6}',
                   'State: , Scope: \nState: (?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: \n'
                    'State: a, Scope: \nState: abb{2,9}c{0,5}d{0,6}, Scope: \n'
                    'State: abb{2,9}c{0,5}d{0,6}(?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: \n'
                    'State: abcd, Scope: \nState: a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6}, Scope: \n'
                    'State: a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6}(?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: a{1,3}\n'
                    'State: b, Scope: \nState: bb{2,9}c{0,5}d{0,6}, Scope: \n'
                    'State: bb{2,9}c{0,5}d{0,6}(?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: \n'
                    'State: bcd, Scope: \nState: b{2,9}abb{2,9}c{0,5}d{0,6}, Scope: \n'
                    'State: b{2,9}abb{2,9}c{0,5}d{0,6}(?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: b{2,9}\n'
                    'State: b{2,9}c{0,5}d{0,6}, Scope: \n'
                    'State: b{2,9}c{0,5}d{0,6}(?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: b{2,9}1\n'
                    'State: c, Scope: \nState: cd, Scope: \nState: c{0,5}d{0,6}, Scope: \n'
                    'State: c{0,5}d{0,6}(?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: c{0,5}\n'
                    'State: d, Scope: \nState: d{0,6}, Scope: \n'
                    'State: d{0,6}(?:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd, Scope: d{0,6}',
                   'State: , Scope: \nState: (?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: \n'
                    'State: a, Scope: \nState: abb{2,9}c{1,5}d{1,6}, Scope: \n'
                    'State: abb{2,9}c{1,5}d{1,6}(?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: \n'
                    'State: abcd, Scope: \nState: a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6}, Scope: \n'
                    'State: a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6}(?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: a{1,3}\n'
                    'State: b, Scope: \nState: bb{2,9}c{1,5}d{1,6}, Scope: \n'
                    'State: bb{2,9}c{1,5}d{1,6}(?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: \n'
                    'State: bcd, Scope: \nState: b{2,9}abb{2,9}c{1,5}d{1,6}, Scope: \n'
                    'State: b{2,9}abb{2,9}c{1,5}d{1,6}(?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: b{2,9}\n'
                    'State: b{2,9}c{1,5}d{1,6}, Scope: \n'
                    'State: b{2,9}c{1,5}d{1,6}(?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: b{2,9}1\n'
                    'State: c, Scope: \nState: cd, Scope: \nState: c{1,5}d{1,6}, Scope: \n'
                    'State: c{1,5}d{1,6}(?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: c{1,5}\n'
                    'State: d, Scope: \nState: d{1,6}, Scope: \n'
                    'State: d{1,6}(?:a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})*abcd, Scope: d{1,6}', 
                   'State: , Scope: \nState: (?:[^\\n]{9})*, Scope: \nState: [^\\n], Scope: \n'
                    'State: [^\\n]{9}, Scope: \nState: [^\\n]{9}(?:[^\\n]{9})*, Scope: [^\\n]{9}'
                    ]


# This variables will be used in other files
COMPILATION_FLAG = 'SCOPETESTS'
START_OF_SOURCE_FILE = '#include <re2/re2.h>\n#include <iostream>\nint main() {\nRE2::Options opt;\nopt.set_use_CsAs(true);\n'
END_OF_SOURCE_FILE = '}'
DELIMITER = ';,;,;;,,;;,,'
TEST_NAMES = ['Patterns without scope', 'Patterns with scope']
PRINT_DELIMITER_COMMAND = f'std::cout << \"{DELIMITER}\" << std::endl;\n'
LIST_OF_ALL_PATTERNS = [list_of_no_scope_patterns, list_of_patterns]
LIST_OF_ALL_OUTPUTS = [list_of_no_scope_outputs, list_of_outputs]