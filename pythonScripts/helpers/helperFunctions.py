# Author: Michal Horky (xhorky23)
import fileinput
import subprocess
from pathlib import Path

# Functions to be used by other scripts
def compileWithFlag(flag):
    '''
    Function firstly insert flag specified by the function parameter into a Makefile then compile re2
    and delete the flag, so the Makefile can be used by other scripts
    '''
    if not addPythonTestFlag(flag):
        return False
    if flag:
        print(f'\033[94mCOMPILING WITH {flag} FLAG')
    else:
        print('\033[94mCOMPILING WITHOUT FLAG')
    compileRE2()
    deletePythonTestFlag(flag)
    return True

# Functions for internal usage

def addPythonTestFlag(flag):
    '''
    Function add a flag to Makefile
    @param flag - a flag to be added
    '''
    if not flag:
        return True
    try:
        for line in fileinput.FileInput('../re2-master/Makefile', inplace=1):
            if line == '# PYTHONTEST=-D \n':
                print('PYTHONTEST=-D ' + flag)
            else:
                print(line, end='')
    except FileNotFoundError:
        print("Please run the script in the pythonScripts directory")
        return False
    return True

def deletePythonTestFlag(flag):
    '''
    Function delete a flag from Makefile so another flag can be inserted later
    @param flag - a flag to be deleted
    '''
    if not flag:
        return
    for line in fileinput.FileInput('../re2-master/Makefile', inplace=1):
        if line == 'PYTHONTEST=-D ' + flag + '\n':
            print('# PYTHONTEST=-D ')
        else:
            print(line, end='')

def compileRE2():
    '''
    Function compile re2 using install script
    If there is no change in the source code, make will not re-compile re2 with an inserted flag, so make clean must
    be called first
    '''
    make_clean_proc = subprocess.Popen(['sudo', 'make', 'clean'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd='../re2-master/')
    make_clean_proc.communicate()
    # For checking compilation error, ./install always returns 0 because make testinstall always return 0
    check_make_errors_proc = subprocess.Popen(['sudo', 'make'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd='../re2-master/')
    error_text = check_make_errors_proc.stderr.read()   
    check_make_errors_proc.communicate()
    if check_make_errors_proc.returncode != 0:
        raise RuntimeError("RE2 COMPILATION FAILED, ERROR:\n"+error_text.decode())
    make_proc = subprocess.Popen(['sh', './install.sh'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd='../')
    make_proc.communicate()

def clean(cwd):
    '''
    Calling make clean so no compiled files remain in the folder
    Calling make clean for re2, so the next "make" will be done without a test flag
    '''
    make_clean_proc = subprocess.Popen(['make', 'clean'], stdout=subprocess.PIPE, cwd=cwd)
    make_clean_proc.communicate()
    make_clean_re2_proc = subprocess.Popen(['sudo', 'make', 'clean'], stdout=subprocess.PIPE, cwd='../re2-master/')
    make_clean_re2_proc.communicate()


def uncoment_unanchored_match_settings():
    for fileObject in Path("benchmarks").iterdir():
        if fileObject.name == "Makefile":
            continue
        for line in fileinput.FileInput(fileObject.absolute(), inplace=1):
            if line == '    //opt.set_unanchored(true);\n':
                print('    opt.set_unanchored(true);')
            else:
                print(line, end='')

def coment_unanchored_match_settings():
    for fileObject in Path("benchmarks").iterdir():
        if fileObject.name == "Makefile":
            continue
        for line in fileinput.FileInput(fileObject.absolute(), inplace=1):
            if line == '    opt.set_unanchored(true);\n':
                print('    //opt.set_unanchored(true);')
            else:
                print(line, end='')


def importConstansts(typeOfConstants):
    global constants
    if typeOfConstants == 'equation_tests':
        from helpers import constantsEquationsTests as constants
    elif typeOfConstants == 'derivatives_tests':
        from helpers import constantsDerivativesTests as constants
    elif typeOfConstants == 'normalization_tests':
        from helpers import constantsNormalizationTests as constants
    elif typeOfConstants == 'ca_final_states_tests':
        from helpers import constantsCaFinalStatesTests as constants
    elif typeOfConstants == 'scope_tests':
        from helpers import constantsScopeTests as constants
    elif typeOfConstants == 'gamma_set_tests':
        from helpers import constantsGammaSetTests as constants
    elif typeOfConstants == 'determinization_tests':
        from helpers import constantsDeterminizationTests as constants
    elif typeOfConstants == 'csa_final_states_tests':
        from helpers import constantsCsaFinalStatesTests as constants
    elif typeOfConstants == 'matching_tests' or typeOfConstants == 'matching_unanchored_tests':
        from helpers import constantsMatchingTests as constants
    elif typeOfConstants == 'nondeterministic_benchmarks':
        from helpers import constantsNondeterministicBenchmarks as constants
    elif typeOfConstants == 'deterministic_benchmarks':
        from helpers import constantsDeterministicBenchmarks as constants