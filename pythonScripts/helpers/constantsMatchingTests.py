'''
Author: Michal Horky (xhorky23)
Patterns for the matching tests
'''


# Partial lists are only local


# Patterns for the source file for full match
list_of_patterns = ['RE2 pattern(\"a{1,3}\", opt);\nRE2 pattern1(\"a{1,3}\");\n',
                    'RE2 pattern(\"b{2,300}\", opt);\nRE2 pattern1(\"b{2,300}\");\n', 
                    'RE2 pattern(\"ab{2,300}\", opt);\nRE2 pattern1(\"ab{2,300}\");\n',
                    'RE2 pattern(\"(ab){2,300}\", opt);\nRE2 pattern1(\"(ab){2,300}\");\n',
                    'RE2 pattern(\"a(ab){2,300}\", opt);\nRE2 pattern1(\"a(ab){2,300}\");\n',
                    'RE2 pattern(\"a{1,3}c\", opt);\nRE2 pattern1(\"a{1,3}c\");\n',
                    'RE2 pattern(\"b{2,300}c\", opt);\nRE2 pattern1(\"b{2,300}c\");\n',
                    'RE2 pattern(\"ab{2,300}c\", opt);\nRE2 pattern1(\"ab{2,300}c\");\n',
                    'RE2 pattern(\"(ab){2,300}c\", opt);\nRE2 pattern1(\"(ab){2,300}c\");\n',
                    'RE2 pattern(\"a(ab){2,300}c\", opt);\nRE2 pattern1(\"a(ab){2,300}c\");\n',
                    'RE2 pattern(\"a(ab){2,300}qr\", opt);\nRE2 pattern1(\"a(ab){2,300}qr\");\n',
                    'RE2 pattern(\"a{1,3}b{1,3}ab\", opt);\nRE2 pattern1(\"a{1,3}b{1,3}ab\");\n',
                    'RE2 pattern(\"a{0,3}b{0,3}ab\", opt);\nRE2 pattern1(\"a{0,3}b{0,3}ab\");\n',
                    'RE2 pattern(\".*a{1,3}b{1,3}a\", opt);\nRE2 pattern1(\".*a{1,3}b{1,3}a\");\n',
                    'RE2 pattern(\".*a{0,3}b{0,3}a\", opt);\nRE2 pattern1(\".*a{0,3}b{0,3}a\");\n',
                    'RE2 pattern(\"a{1,3}c{5,9}ab\", opt);\nRE2 pattern1(\"a{1,3}c{5,9}ab\");\n',
                    'RE2 pattern(\"a{1,3}b{5,9}ab\", opt);\nRE2 pattern1(\"a{1,3}b{5,9}ab\");\n',
                    'RE2 pattern(\"a{1,3}b{0,9}ab\", opt);\nRE2 pattern1(\"a{1,3}b{0,9}ab\");\n',
                    'RE2 pattern(\"a{0,3}b{0,9}ab\", opt);\nRE2 pattern1(\"a{0,3}b{0,9}ab\");\n',
                    'RE2 pattern(\"a{0,3}b{0,9}abb{0,9}\", opt);\nRE2 pattern1(\"a{0,3}b{0,9}abb{0,9}\");\n',
                    'RE2 pattern(\"a{1,3}b{2,9}abb{2,9}\", opt);\nRE2 pattern1(\"a{1,3}b{2,9}abb{2,9}\");\n',
                    'RE2 pattern(\"a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6}\", opt);\nRE2 pattern1(\"a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6}\");\n',
                    'RE2 pattern(\"(a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd\", opt);\nRE2 pattern1(\"(a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd\");\n',
                    'RE2 pattern(\"(a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})+abcd\", opt);\nRE2 pattern1(\"(a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})+abcd\");\n',
                    'RE2 pattern(\"(?s:Regex.Replace(text, @\\".+[a-zA-Z0-9]{17,500}.+(\\r\\n)\\", Environment.NewLine, RegexOptions.Compiled))\", opt);\nRE2 pattern1(\"(?s:Regex.Replace(text, @\\".+[a-zA-Z0-9]{17,500}.+(\\r\\n)\\", Environment.NewLine, RegexOptions.Compiled))\");\n',
                    'RE2 pattern(\"(?s:^QS([NDR])(.{4})(.{6})(\\\d{8})(\\\d{8}| {8})([01]{7})([ SH])([ ABXG])(.{4})(.{6})(.{8})(.{8})(.)$)\", opt);\nRE2 pattern1(\"(?s:^QS([NDR])(.{4})(.{6})(\\\d{8})(\\\d{8}| {8})([01]{7})([ SH])([ ABXG])(.{4})(.{6})(.{8})(.{8})(.)$)\");\n',
                    'RE2 pattern(\"(?s:(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{5})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{4})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{3})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{2})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{1}))\", opt);\nRE2 pattern1(\"(?s:(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{5})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{4})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{3})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{2})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{1}))\");\n',
                    'RE2 pattern(\"(?s:rise (\\\d{4}) set (\\\d{4}).*(\\\d{2})h (\\\d{2})m (\\\d{2})s - (\\\d)m (\\\d{2})s (\\\d{3})\\\", \\\"rise \\\d{4} set \\\d{4}.*\\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3}\\\", \\\"rise (\\\d*) set (\\\d*) .* (\\\d*)h (\\\d*)m (\\\d*)s - (\\\d*)m (\\\d*)s (\\\d*)\\\", \\\"rise \\\d{4} set \\\d{4} \\\(:\\\) \\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3})\", opt);\nRE2 pattern1(\"(?s:rise (\\\d{4}) set (\\\d{4}).*(\\\d{2})h (\\\d{2})m (\\\d{2})s - (\\\d)m (\\\d{2})s (\\\d{3})\\\", \\\"rise \\\d{4} set \\\d{4}.*\\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3}\\\", \\\"rise (\\\d*) set (\\\d*) .* (\\\d*)h (\\\d*)m (\\\d*)s - (\\\d*)m (\\\d*)s (\\\d*)\\\", \\\"rise \\\d{4} set \\\d{4} \\\(:\\\) \\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3})\");\n'
                    ]

# Patterns for unanchored match
list_of_patterns_unanchored = ['RE2 pattern(\"(?s:fa{1,3})\", opt);\nRE2 pattern1(\"(?s:fa{1,3})\");\n',
                               'RE2 pattern(\"(?s:fb{2,300})\", opt);\nRE2 pattern1(\"(?s:fb{2,300})\");\n', 
                               'RE2 pattern(\"(?s:wb{2,300})\", opt);\nRE2 pattern1(\"(?s:wb{2,300})\");\n',
                               'RE2 pattern(\"(?s:f(ab){2,300})\", opt);\nRE2 pattern1(\"(?s:f(ab){2,300})\");\n',
                               'RE2 pattern(\"(?s:w(ab){2,300})\", opt);\nRE2 pattern1(\"(?s:w(ab){2,300})\");\n',
                               'RE2 pattern(\"(?s:va{1,3})\", opt);\nRE2 pattern1(\"(?s:va{1,3}1)\");\n',
                               'RE2 pattern(\"(?s:b{2,300})\", opt);\nRE2 pattern1(\"(?s:b{2,300})\");\n',
                               'RE2 pattern(\"(?s:ab{2,300})\", opt);\nRE2 pattern1(\"(?s:ab{2,300})\");\n',
                               'RE2 pattern(\"(?s:(ab){2,300}1)\", opt);\nRE2 pattern1(\"(?s:(ab){2,300}1)\");\n',
                               'RE2 pattern(\"(?s:a(ab){2,300}1)\", opt);\nRE2 pattern1(\"(?s:a(ab){2,300}1)\");\n',
                               'RE2 pattern(\"(?s:a(ab){2,300}qr)\", opt);\nRE2 pattern1(\"(?s:a(ab){2,300}qr)\");\n',
                               'RE2 pattern(\"(?s:aa{1,3}b{1,3}ab)\", opt);\nRE2 pattern1(\"(?s:aa{1,3}b{1,3}ab)\");\n',
                               'RE2 pattern(\"(?s:a{0,3}b{0,3}ab)\", opt);\nRE2 pattern1(\"(?s:a{0,3}b{0,3}ab)\");\n',
                               'RE2 pattern(\"(?s:.*a{1,3}b{1,3}1)\", opt);\nRE2 pattern1(\"(?s:.*a{1,3}b{1,3}1)\");\n',
                               'RE2 pattern(\"(?s:.*a{0,3}b{0,3}1)\", opt);\nRE2 pattern1(\"(?s:.*a{0,3}b{0,3}1)\");\n',
                               'RE2 pattern(\"(?s:a{1,3}c{5,9}ab)\", opt);\nRE2 pattern1(\"(?s:a{1,3}c{5,9}ab)\");\n',
                               'RE2 pattern(\"(?s:a{1,3}b{5,9}ab)\", opt);\nRE2 pattern1(\"(?s:a{1,3}b{5,9}ab)\");\n',
                               'RE2 pattern(\"(?s:aa{1,3}b{0,9}ab)\", opt);\nRE2 pattern1(\"(?s:aa{1,3}b{0,9}ab)\");\n',
                               'RE2 pattern(\"(?s:a{0,3}b{0,9}ab)\", opt);\nRE2 pattern1(\"(?s:a{0,3}b{0,9}ab)\");\n',
                               'RE2 pattern(\"(?s:a{0,3}b{0,9}abb{0,9})\", opt);\nRE2 pattern1(\"(?s:a{0,3}b{0,9}abb{0,9})\");\n',
                               'RE2 pattern(\"(?s:a{1,3}b{2,9}abb{2,9})\", opt);\nRE2 pattern1(\"(?s:a{1,3}b{2,9}abb{2,9})\");\n',
                               'RE2 pattern(\"(?s:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})\", opt);\nRE2 pattern1(\"(?s:a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})\");\n',
                               'RE2 pattern(\"(?s:(a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd)\", opt);\nRE2 pattern1(\"(?s:(a{1,3}b{2,9}abb{2,9}c{0,5}d{0,6})*abcd)\");\n',
                               'RE2 pattern(\"(?s:(a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})+abcd)\", opt);\nRE2 pattern1(\"(?s:(a{1,3}b{2,9}abb{2,9}c{1,5}d{1,6})+abcd)\");\n',
                               'RE2 pattern(\"(?s:Regex.Replace(text, @\\".+[a-zA-Z0-9]{17,500}.+(\\r\\n)\\", Environment.NewLine, RegexOptions.Compiled))\", opt);\nRE2 pattern1(\"(?s:Regex.Replace(text, @\\".+[a-zA-Z0-9]{17,500}.+(\\r\\n)\\", Environment.NewLine, RegexOptions.Compiled))\");\n',
                               'RE2 pattern(\"(?s:^QS([NDR])(.{4})(.{6})(\\\d{8})(\\\d{8}| {8})([01]{7})([ SH])([ ABXG])(.{4})(.{6})(.{8})(.{8})(.)$)\", opt);\nRE2 pattern1(\"(?s:^QS([NDR])(.{4})(.{6})(\\\d{8})(\\\d{8}| {8})([01]{7})([ SH])([ ABXG])(.{4})(.{6})(.{8})(.{8})(.)$)\");\n',
                               'RE2 pattern(\"(?s:(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{5})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{4})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{3})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{2})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{1}))\", opt);\nRE2 pattern1(\"(?s:(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{5})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{4})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{3})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{2})|(\\\d{1}-\\\d{2}\\\s*)(of +)(\\\s?\\\d{1}))\");\n',
                               'RE2 pattern(\"(?s:rise (\\\d{4}) set (\\\d{4}).*(\\\d{2})h (\\\d{2})m (\\\d{2})s - (\\\d)m (\\\d{2})s (\\\d{3})\\\", \\\"rise \\\d{4} set \\\d{4}.*\\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3}\\\", \\\"rise (\\\d*) set (\\\d*) .* (\\\d*)h (\\\d*)m (\\\d*)s - (\\\d*)m (\\\d*)s (\\\d*)\\\", \\\"rise \\\d{4} set \\\d{4} \\\(:\\\) \\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3})\", opt);\nRE2 pattern1(\"(?s:rise (\\\d{4}) set (\\\d{4}).*(\\\d{2})h (\\\d{2})m (\\\d{2})s - (\\\d)m (\\\d{2})s (\\\d{3})\\\", \\\"rise \\\d{4} set \\\d{4}.*\\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3}\\\", \\\"rise (\\\d*) set (\\\d*) .* (\\\d*)h (\\\d*)m (\\\d*)s - (\\\d*)m (\\\d*)s (\\\d*)\\\", \\\"rise \\\d{4} set \\\d{4} \\\(:\\\) \\\d{2}h \\\d{2}m \\\d{2}s - \\\dm \\\d{2}s \\\d{3})\");\n'
                               ]

# List of string used for matching
list_of_strings = ['a', 'aa', 'aaa', 'b', 'ab', 'abb', 'bb', 'abab', 'aab', 'aabab', 'aababab' 'ac', 'aac', 'aaac',
                   'bc', 'bbc', 'bbbbbbbbbbbbbbc', 'abc', 'abababababababc', 'aababc',  'aababababababababqr', 'aaabbbab',
                   'ljafjgdsafjaba', 'kajdshfkhdsafaaabbba', 'lkdsajflkjdsflkjdsakfa', 'poqwiepoqwiepowqiepoia',
                   'acccccab', 'aaacccccccccab', 'abbbbbab', 'aaaab', 'aaabbbbbbab', 'pppppppppppppppppppppppppppppppppppp',
                   'aaabbbbbbbbbab', 'aaabbbabbbb', 'aaabbbbbbbbbabbbbbbbbbb', 'poiuytrewpoiuytrewpoiuytrewpoiuytrewpoiuyt',
                   'abbabbb', 'abbabbbcd', 'aaabbbbbbbbbabbbbbbbbbbcccccdddddd', 'abcd', 'aaabbbbbbbbbabbbbbbbbbbcccccddddddabcd',
                   'abbabbbabcd', 'abbabbbcccdddabcd', 'abbabbbccddabcd', 'aaabbbbbbbbbabbbbbbbbbbcdabcd', '', 'qwertyuio', 
                   'qwertyuiolkjhgfdsa', 'qazwsxedcrfvtgbyhnujmik,ol.', '234567890poiuytrewqasdfghjklmnbvcxz', 'ppppppqowu',
                   'alpqowieurythgjfkdl', 'pqoelsjdkryestqalsifgehrts', 'pqoelsjdkryestqalsifgehrtspp',
                   "Regex.Replacetext, @\\\"pqlkeiwkqiqkAAAAAAAAAAAAAAAAAApqpqpqp\\r\\n\\\", Environment.NewLine, RegexOptions.Compiled",
                   "Regex.Replacetext, @\\\"pqlkeiwkqiqkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApqpqpqp\\r\\n\\\", Environment.NewLine, RegexOptions.Compiled",
                   "QSDaaaassssss88888888000000000000000SA44446666668888888899999999a", "QSDaaaassssss88888888        0000000  44446666668888888899999999a",
                   "BQSDaaaassssss88888888        0000000  44446666668888888899999999aB", "1-22of 22222", "9-88  of  9",
                   "9-88        of           99", "9-88        of           999", "9-88        of           9999", "9-88        of           99999", 
                   "9-88        of           999999", 
                   "rise 1111 set 1111            22h 22m 33s - 2m 22s 000\\\", \\\"rise 2222 set 0000      11h 22m 22s - 8m 00s 000\\\", \\\"rise  set 999999999999999999 aksjdhfkajshdfkhsadfkhdsafkh 2222222h m s - m 88888s 8128281\\\", \\\"rise 0987 set 0987 (:) 22h 66m 00s - 9m 99s 098",
                   "rise 1111 set 1111                                   22h 22m 33s - 2m 22s 000\\\", \\\"rise 2222 set 0000      11h 22m 22s - 8m 00s 000\\\", \\\"rise  set 999999999999999999 aksjdhfkalksfdkahfdkhdsfkhfdskajdsajshdfkhsadfkhdsafkh 2222983247928347983274982374982374982374222h m s - m 88888s 8128281\\\", \\\"rise 0987 set 0987 (:) 22h 66m 00s - 9m 99s 098"]


# This variables will be used in other files
COMPILATION_FLAG = ''
START_OF_SOURCE_FILE = '#include <re2/re2.h>\n#include <iostream>\nint main() {\nbool testFail = false;\nRE2::Options opt;\nopt.set_use_CsAs(true);\n'
START_OF_SOURCE_FILE_UNANCHORED = '#include <re2/re2.h>\n#include <iostream>\nint main() {\nbool testFail = false;\nRE2::Options opt;\nopt.set_use_CsAs(true);\nopt.set_unanchored(true);\n'
END_OF_SOURCE_FILE = 'if(!testFail) {\n std::cout << "\t\033[1;32msucceed" << std::endl;}\n}'
TEST_NAMES = ['Matching']
LIST_OF_ALL_PATTERNS = [list_of_patterns]
LIST_OF_ALL_PATTERNS_UNANCHORED = [list_of_patterns_unanchored]
LIST_OF_STRINGS = [list_of_strings]
