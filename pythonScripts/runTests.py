# Author: Michal Horky (xhorky23)
import subprocess
from helpers import helperFunctions
import argparse

# When set to True, the code won't do any comparations, it will only print out the output of runned programs
DEBUG = False

def run():
    # Run all tests
    for test_name, pattern_list, output_list in zip(constants.TEST_NAMES, constants.LIST_OF_ALL_PATTERNS, constants.LIST_OF_ALL_OUTPUTS):
        run_single_test(test_name, pattern_list, output_list)

def runMatchingTests(unanchored=False):
    if unanchored:
        patterns = constants.LIST_OF_ALL_PATTERNS_UNANCHORED
    else:
        patterns = constants.LIST_OF_ALL_PATTERNS
    for test_name, pattern_list, string_list in zip(constants.TEST_NAMES, patterns, constants.LIST_OF_STRINGS):
        # Print all used strings
        print('\033[94m' + test_name)
        print('\033[0mStrings used for matching:')
        for test_string in string_list:
            print(f'\033[0m\t{test_string}')
        # Create a C++ source with all the patters    
        for pattern in pattern_list:
            source_file = open('tests/test.cc', 'w')
            if unanchored:
                source_file.write(constants.START_OF_SOURCE_FILE_UNANCHORED)        
            else:
                source_file.write(constants.START_OF_SOURCE_FILE)
            # Print the pattern
            source_file.write(f'std::cout << "\033[0mpattern: " << {pattern.splitlines()[0][12:-7]} << " " << std::endl;\n')
            source_file.write(pattern)
            # Add comparison of the new algorithm and the original algorithm to check if the new algorithm computes the same output
            for test_string in string_list:
                if unanchored:
                    source_file.write(f'if(RE2::PartialMatch("{test_string}", pattern) != RE2::PartialMatch("{test_string}", pattern1)) {{\n std::cout << "\t" << "string: {test_string} \033[1;31mfail" << "\033[0m" << std::endl;\ntestFail = true;\n}}\n')
                else:
                    source_file.write(f'if(RE2::FullMatch("{test_string}", pattern) != RE2::FullMatch("{test_string}", pattern1)) {{\n std::cout << "\t" << "string: {test_string} \033[1;31mfail" << "\033[0m" << std::endl;\ntestFail = true;\n}}\n')
            source_file.write(constants.END_OF_SOURCE_FILE)
            source_file.close()
            make_proc = subprocess.Popen(["make"], stdout=subprocess.PIPE, cwd='tests')
            make_proc.communicate()
            subprocess.Popen(['./test'], cwd='tests')
            p = subprocess.Popen(['./test'], stdout=subprocess.PIPE, cwd='tests')
            _, err = p.communicate()
            if err:
                print(err)

def run_single_test(test_name, patterns, correct_outputs):
    # Create a C++ source file
    source_file = open('tests/test.cc', 'w')
    source_file.write(constants.START_OF_SOURCE_FILE)
    for pattern in patterns:
        source_file.write(pattern)
        source_file.write(constants.PRINT_DELIMITER_COMMAND)
    source_file.write(constants.END_OF_SOURCE_FILE)
    source_file.close()
    make_proc = subprocess.Popen(["make"], stdout=subprocess.PIPE, cwd='tests')
    make_proc.communicate()
    print('\033[94m' + test_name)
    p = subprocess.Popen(['./test'], stdout=subprocess.PIPE, cwd='tests')
    stdout, _ = p.communicate()
    output_lines_list = stdout.decode().splitlines()
    num_of_tests = len(patterns)
    num_of_passed_tests = 0
    # Check the individual outputs
    for pattern, correct_output in zip(patterns, correct_outputs):
        pattern_for_output = pattern[pattern.find('"')+1:]
        pattern_for_output = pattern_for_output[:pattern_for_output.find('"')]
        # After each output, DELIMITER is printed, get all lines before, and then delete these lines plus DELIMITER
        lines_to_check = output_lines_list[:output_lines_list.index(constants.DELIMITER)]
        output_lines_list = output_lines_list[output_lines_list.index(constants.DELIMITER)+1:]
        if DEBUG:
            print('Pattern: \"' + pattern_for_output + '\"')
            print('\n'.join(lines_to_check))
            print('\n\n\n')
            continue
        if(lines_to_check == correct_output.splitlines()):
            num_of_passed_tests += 1
            print('\t' + '\033[92m' + 'OK, pattern: \"' + pattern_for_output + '\"')
        else:
            print('\t' + '\033[91m' + 'FAIL on pattern: \"' + pattern_for_output + '\"')
            print('\t EXPECTED: \n', correct_output)
            print('\t PROGRAM OUTPUT: \n', '\n'.join(lines_to_check))
    if num_of_passed_tests == num_of_tests:
        print('\t' + '\033[92m' + 'ALL TEST PASSED')
    else:
        print('\033[91m' + 'PASSED: ' + str(num_of_passed_tests) + ' OUT OF ' + str(num_of_tests))

parser = argparse.ArgumentParser(description='Run test of the Counting-Set Automata part of the RE2')
parser.add_argument('-t','--tests', nargs='+', help='A list of the names of the tests to run', required=False, 
                    choices=['equation', 'derivatives', 'normalization', 'ca_final_states', 'scope', 'gamma_set', 'determinization', 'csa_final_states', 'matching', 'matching_unanchored'])

args = parser.parse_args()

if not args.tests:
    # Keep synced with the new tests
    allTestNames = ['equation', 'normalization', 'derivatives', 'ca_final_states', 'scope', 'gamma_set', 'determinization', 'csa_final_states', 'matching', 'matching_unanchored']
else:
    allTestNames = args.tests

for typeOfTest in allTestNames:
    helperFunctions.importConstansts(f"{typeOfTest}_tests")
    constants = helperFunctions.constants
    badFolderError = False
    try:
        if helperFunctions.compileWithFlag(constants.COMPILATION_FLAG):
            if typeOfTest == "matching":
                runMatchingTests()
            elif typeOfTest == "matching_unanchored":
                runMatchingTests(True)
            else:
                run()
        else:
            badFolderError = True
            break
    except RuntimeError as e:
        print(f'\033[91m{e}')
        helperFunctions.deletePythonTestFlag(constants.COMPILATION_FLAG)
        break
    finally:
        if not badFolderError:
            helperFunctions.clean('tests')
            helperFunctions.deletePythonTestFlag(constants.COMPILATION_FLAG)
