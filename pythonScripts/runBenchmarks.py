# Author: Michal Horky (xhorky23)

import subprocess
from helpers import helperFunctions
import argparse
from pathlib import Path
from datetime import datetime

def run(regexes, timeout_ms, output_prefix):
    file_names = []
    for fileObject in Path("benchmarks").iterdir():
        if fileObject.name == "Makefile":
            continue
        file_names.append(fileObject.name[:-3])   
    # Regexes in constants are in same order as files (01, 02, 03, etc.) so to print regexes, the files must be 
    # executed in same order
    file_names.sort()
    make_proc = subprocess.Popen(["make"], stdout=subprocess.PIPE, cwd='benchmarks')
    make_proc.communicate()
    timeouts = 0
    time_total = 0
    # Run all the files with patterns, compute timeouts and overall time
    for regex, file_name in zip(regexes, file_names):
        timeout, run_time = run_single_file(regex, file_name, timeout_ms, output_prefix)
        timeouts += int(timeout)
        time_total += run_time
    print(f"{len(regexes)} benchmarks completed")
    print(f"Timeouts: {timeouts}")
    print(f"The total time of the remaining {len(regexes)-timeouts} benchmarks is {time_total}")

def run_single_file(regex, file_name, timeout_ms, output_prefix):
    # Use the timeout
    timeout_s = timeout_ms/1000
    try:
        print(f"\033[94mRegex: {regex}")
        output = subprocess.check_output(f"./benchmarks/{file_name}", timeout=timeout_s)
        output_string = output.decode('utf-8')
        print(f"\033[92m{output_prefix}{output_string} ms")
        return False, int(output_string)
    except subprocess.TimeoutExpired as e:
        print(f'\033[91m{e}')
        return True, 0
    except Exception as e:
        print(f'\033[91m{e}')
        return True, 0
    

parser = argparse.ArgumentParser(description='Run the benchmarks on some of the regexes from the paper by Turoňová et al.')
parser.add_argument('--nca', dest='nondeterministic', action='store_true', help='Use to run benchmarks only for creating nondeterministic CA')
parser.set_defaults(nondeterministic=False)
parser.add_argument('--dca', dest='deterministic', action='store_true', help='Use to run benchmarks only for creating deterministic CsA')
parser.set_defaults(deterministic=False)
parser.add_argument('--unanchored', dest='unanchored', action='store_true', help='Use to use .* as a prefix and as a sufix to all regexes')
parser.set_defaults(deterministic=False)


args = parser.parse_args()


# Run all benchmarks if no flag is specified
if not args.nondeterministic and not args.deterministic:
    args.nondeterministic = args.deterministic = True
badFolderError = False

# Prepare and run bechmarks
if args.nondeterministic:
    helperFunctions.importConstansts('nondeterministic_benchmarks')
    constants = helperFunctions.constants
    try:
        if helperFunctions.compileWithFlag(constants.COMPILATION_FLAG):
            print(f"\033[94mTimeout is set to {constants.TIMEOUT_MS} ms")
            run(constants.REGEXES, constants.TIMEOUT_MS, constants.OUTPUT_PREFIX)
        else:
            badFolderError = True
    except RuntimeError as e:
        print(f'\033[91m{e}')
        helperFunctions.deletePythonTestFlag(constants.COMPILATION_FLAG)
    finally:
        if not badFolderError:
            now = datetime.now()
            now.strftime("%H:%M:%S")
            print("Time before clean", now.strftime("%H:%M:%S"))
            helperFunctions.deletePythonTestFlag(constants.COMPILATION_FLAG)
            helperFunctions.clean('benchmarks')
            if args.unanchored:
                helperFunctions.coment_unanchored_match_settings()

if args.nondeterministic and args.deterministic:
    print("\n")

if args.deterministic:
    helperFunctions.importConstansts('deterministic_benchmarks')
    constants = helperFunctions.constants
    try:
        if helperFunctions.compileWithFlag(constants.COMPILATION_FLAG):
            print(f"\033[94mTimeout is set to {constants.TIMEOUT_MS} ms")
            run(constants.REGEXES, constants.TIMEOUT_MS, constants.OUTPUT_PREFIX)
        else:
            badFolderError = True
    except RuntimeError as e:
        print(f'\033[91m{e}')
        helperFunctions.deletePythonTestFlag(constants.COMPILATION_FLAG)
    finally:
        if not badFolderError:
            now = datetime.now()
            now.strftime("%H:%M:%S")
            print("Time before clean", now.strftime("%H:%M:%S"))
            helperFunctions.clean('benchmarks')
            helperFunctions.deletePythonTestFlag(constants.COMPILATION_FLAG)
            if args.unanchored:
                helperFunctions.coment_unanchored_match_settings()
