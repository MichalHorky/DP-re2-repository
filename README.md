# Overview
This folder contains RE2 with the new implementation of CsA-based matching. There are executable files for both Windows and Linux and also source files of the RE2. The source files can be used to compile RE2. It can then be used in C++ code for regular expression matching. There are also Python scripts that can be used to run tests and benchmarks written. The folder also contains text of the thesis in PDF format and Latex source files of the text of the thesis.


# Using the Executable Files
The executable files are `find_lines_csa` and `find_lines_csa.exe` for Linux and Windows, respectively. The files can be run with two arguments, the first is the regular expression, and the second is the file. The output will be a number of lines on which a string matching the regular expression was found. The matching string can be anywhere on the line (i.e., `.*` is used as prefix and suffix for the input regex).

For example, the output of the `./find_lines_csa "^#.{10,300}" README.md` will be `4` as there are four headings at least ten characters long.


# Compiling the RE2
The RE2 can be compiled using the `sudo ./install.sh` command. Note that it must be run with `sudo`. This command will run a script (`re2-master/makeScript.sh`) that compiles RE2 and creates a `.so` file so the RE2 can be used in C++ code.

# Using CsA-Based Matching in C++ Code
The following header must be included to used the RE2 in C++ code: `<re2/re2.h>`. The usage of CsA-based matching is a little bit different than the usage of the original RE2 functions. The usage of CsA-based matching is determined by the `RE2::Options` object passed to the `RE2` object constructor. The options object with the setting that will trigger the usage of CsA-based settings can be created by the following commands:

`RE2::Options opt;`

`opt.set_use_CsAs(true);`

With such a setting, the CsA-based algorithm will be used for matching. However, with such a setting, only the whole input string must match the regular expression. For example, the regex `a{1,3}` will match the string `aaa` but not the string `aaab` since only a part of the string matches the regex. 

The pattern can be search anywhere in the input string using another option. It can be set by the following command:

`opt.set_unanchored(true);`

When this command is used, the `.*` will be used as a prefix and suffix of the input regex. That causes that the pattern is searched anywhere in the input string. 

The original RE2 uses `RE2::FullMatch()` and `RE2::PartialMatch()` functions to determine if the pattern must match the whole string or it can be anywhere in the string. For CsA-based matching, these two function does not make any difference, the type of match is set by the settings, and then any of these two functions can be used.

The `RE2` object that can be later used for matching can be created using the following command:

`RE2 pattern_object("a{1,3}", opt);`

The following command is then used to perform the CsA-based matching:

`RE2::FullMatch("aa", pattern_object);`

The return value of this function is `0` if the match fails and `1` if the match succeeds. The CsA-based matching implementation does not support substring extraction as the original RE2. 


# Running the Tests

The tests are supposed to be run on Linux as they compile the RE2 library with corresponding flags.  They will not run on Windows directly but can be run using the [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10). The required version is `Python 3.6`. The main test script (`runTests.py`) is located in the pythonScripts folder. The script has to be run from the pythonScripts folder as it accesses different paths and expects to be run from there. If the script is run without any argument, it will run all tests. It can also be used with an option `-t` or `--tests` followed by the test names. In such a case, it runs the specified tests. The test names are the following:
 + `equation` to run the equations tests,
 + `normalization` to run the normalization tests,
 + `derivatives` to run the derivatives tests,
 + `ca_final_states` to run the CA final states tests,
 + `scope` to run the scope tests,
 + `gamma_set` to run the gamma set tests,
 + `determinization` to run the determinization tests,
 + `csa_final_states` to run the CsA final states tests,
 + `matching` to run the matching tests,
 + `matching_unanchored` to run the unanchored matching tests.
 
 Each of the individual tests compiles the RE2 library with the corresponding flag to print the desired output. It then runs it on a set of regexes and compares the program output with the expected one. It prints out info about the pattern that is tested with the result of the test. If the test fails, it also prints the program output and the expected output.

For example, the command `python3 runTests.py -t equation normalization` will run *equation* tests and *normalization* tests.


# Running the Automaton Creation Benchmarks
The benchmarks are supposed to be run on Linux as they compile the RE2 library with corresponding flags. They will not run on Windows directly but can be run using the [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10). The required version is `Python 3.6`. The main test script (`runBenchmarks.py`) is located in the pythonScripts folder. The script has to be run from the pythonScripts folder as it accesses different
paths and expects to be run from there. If the script is run without any option, it will run all benchmarks. The main script accepts three options:

+ `--nca` to run only the benchmarks for translation of the input regex into CA,
+ `--dca` to run only the benchmarks for determinization of the CA,
+ `--unanchored` to run benchmarks using regexes with .* as a prefix and suffix and with
s flag.

Each of the individual benchmarks compiles the RE2 library with the corresponding flag
to use only the desired part of the algorithm. It then runs it on a set of regexes. For each
of the regexes, it prints out the regex, and the time it took to process it in milliseconds.
After the last regex, it also prints the number of timeouts and the sum of the times.
The timeout of the individual benchmarks is set to 1 800 seconds. It can be changed
using the `TIMEOUT_MS` constant. For the --nca option of the script, the constant has to be
changed in the `constantsNondeterministicBenchmarks.py` file. For the --dca option of
the script, the constant has to be changed in the `constantsDeterministicBenchmarks.py`
file. Both files are located in the /pythonScripts/helpers folder.
