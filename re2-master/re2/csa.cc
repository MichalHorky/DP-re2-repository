//
// Created by Michal Horky (xhorky23) on 21.02.21.
//

#include "csa.h"


#include <iostream>
#include <set>

namespace re2 {
    void CSA::getCsa(Regexp::Derivatives &derivatives, re2::Regexp * initialRegexp, int bytemapRange) {
#if defined(SCOPETESTS)
      this->countingLoopCounter = derivatives.countingLoopCounter;
      this->countingLoopBounds = derivatives.countingLoopBounds;
      this->countingLoops = derivatives.countingLoops;
      this->initialStateString = initialRegexp->ToString();
      this->initialState = derivatives.regexNumberMapping.at(this->initialStateString);
      this->computeStateScopes(derivatives);
      this->preprocessCa(derivatives);
      derivatives.printCaStates();
#elif defined(GAMMASETTESTS)
      if (derivatives.partialDerivatives.empty()) {
        return;
      }
      this->countingLoopCounter = derivatives.countingLoopCounter;
      this->countingLoopBounds = derivatives.countingLoopBounds;
      this->countingLoops = derivatives.countingLoops;
      this->initialStateString = initialRegexp->ToString();
      this->initialState = derivatives.regexNumberMapping.at(this->initialStateString);
      this->computeStateScopes(derivatives);
      this->preprocessCa(derivatives);
      CSA::csaState initialCsaState = {derivatives.caStates[this->initialState]};
      this->stateCounter = 0;
      this->stateIntMap.insert({"", this->stateCounter});
      this->stateCounter++;
      this->stateIntMap.insert({this->initialStateString, this->stateCounter});
      this->stateCounter++;
      std::pair<CSA::csaState, std::string> currentCsaStateToExplore;
      this->csaStates.insert({this->initialStateString, initialCsaState});
      this->csaStatesToExplore.emplace_back(initialCsaState, this->initialStateString);
      this->countingLoopStrings = derivatives.countingLoopStrings;
      this->finalStates.resize(1, {});
      this->finalStates[0] = {{Regexp::Derivatives::True, 0, 0, 0}};
      if (derivatives.finalStates[this->initialState].isSet) {
        this->finalStates.resize(2, {});
        Regexp::Derivatives::finalStateCondition finalCond = derivatives.finalStates.at(this->initialState);
        this->finalStates[1] = {{finalCond.grd, finalCond.countingLoop, finalCond.counting_min, finalCond.counting_max}};
      }
      while (!this->csaStatesToExplore.empty()) {
        currentCsaStateToExplore = csaStatesToExplore.back();
        csaStatesToExplore.pop_back();
        alreadyFoundCsaStates.insert(currentCsaStateToExplore.first);
        for (int i = 0; i < bytemapRange; i++) {
          this->updateGammaSet(currentCsaStateToExplore.first, i, derivatives);
        }
      }
      this->printGammaSet();
#elif defined(DETERMINIZATIONTESTS)
      if (derivatives.partialDerivatives.empty()) {
        return;
      }
      this->countingLoopCounter = derivatives.countingLoopCounter;
      this->countingLoopBounds = derivatives.countingLoopBounds;
      this->countingLoops = derivatives.countingLoops;
      this->initialStateString = initialRegexp->ToString();
      this->initialState = derivatives.regexNumberMapping.at(this->initialStateString);
      this->computeStateScopes(derivatives);
      this->preprocessCa(derivatives);
      CSA::csaState initialCsaState = {derivatives.caStates[this->initialState]};
      this->stateCounter = 0;
      this->stateIntMap.insert({"", this->stateCounter});
      this->stateCounter++;
      this->stateIntMap.insert({this->initialStateString, this->stateCounter});
      this->stateCounter++;
      std::pair<CSA::csaState, std::string> currentCsaStateToExplore;
      this->csaStates.insert({this->initialStateString, initialCsaState});
      this->csaStatesToExplore.emplace_back(initialCsaState, this->initialStateString);
      this->countingLoopStrings = derivatives.countingLoopStrings;
      this->finalStates.resize(1, {});
      this->finalStates[0] = {{Regexp::Derivatives::True, 0, 0, 0}};
      if (derivatives.finalStates[this->initialState].isSet) {
        this->finalStates.resize(2, {});
        Regexp::Derivatives::finalStateCondition finalCond = derivatives.finalStates[this->initialState];
        this->finalStates[1] = {{finalCond.grd, finalCond.countingLoop, finalCond.counting_min, finalCond.counting_max}};
      }
      this->derivatives_var = derivatives;
      while (!this->csaStatesToExplore.empty()) {
        currentCsaStateToExplore = csaStatesToExplore.back();
        csaStatesToExplore.pop_back();
        alreadyFoundCsaStates.insert(currentCsaStateToExplore.first);
        for (int i = 0; i < bytemapRange; i++) {
          this->getNextStateAndTransitions(currentCsaStateToExplore, i);
        }
      }
      this->printBetterCountingSetAutomaton(derivatives);
#elif defined(DETERMINISTICBENCHMARKS)
      // Same as the one used for matching but it does explicit determinization
      if (derivatives.partialDerivatives.empty()) {
        return;
      }
      this->countingLoopCounter = derivatives.countingLoopCounter;
      // Prepare all the information needed to do the on-the-fly determinization
      this->countingLoopBounds = derivatives.countingLoopBounds;
      this->countingLoops = derivatives.countingLoops;
      this->initialStateString = initialRegexp->ToString();
      this->initialState = derivatives.regexNumberMapping.at(this->initialStateString);
      // Scopes are created with the original CA, the transitions that can never be taken are deleted after that
      this->computeStateScopes(derivatives);
      preprocessCa(derivatives);
      CSA::csaState initialCsaState = {derivatives.caStates[this->initialState]};
      // Prepare  the numerical representation of already known states
      this->stateCounter = 0;
      // We insert the epsilon state everytime
      this->stateIntMap.insert({"", this->stateCounter});
      this->stateCounter++;
      this->stateIntMap.insert({this->initialStateString, this->stateCounter});
      this->stateCounter++;
      this->intCsaStatePairVector.resize(2);
      this->intCsaStatePairVector[1] = {initialCsaState, this->initialStateString};
      // The epsilon state is always final, it is the state that is reached when the whole regex is matched (and it does
      // not end with the counting loop)
      this->finalStates.resize(1, {});
      this->finalStates[0] = {{Regexp::Derivatives::True, 0, 0, 0}};
      if (derivatives.finalStates[this->initialState].isSet) {
        this->finalStates.resize(2, {});
        Regexp::Derivatives::finalStateCondition finalCond = derivatives.finalStates.at(this->initialState);
        this->finalStates[1] = {{finalCond.grd, finalCond.countingLoop, finalCond.counting_min, finalCond.counting_max}};
      }
      this->derivatives_var = derivatives;
      this->bytemapRange_var = bytemapRange;
      this->csaStatesToExplore.emplace_back(initialCsaState, this->initialStateString);
      std::pair<CSA::csaState, std::string> currentCsaStateToExplore;
      while (!this->csaStatesToExplore.empty()) {
        currentCsaStateToExplore = csaStatesToExplore.back();
        csaStatesToExplore.pop_back();
        alreadyFoundCsaStates.insert(currentCsaStateToExplore.first);
        for (int i = 0; i < bytemapRange; i++) {
          this->getNextStateAndTransitions(currentCsaStateToExplore, i);
        }
      }
#elif defined(CSAFINALSTATESTESTS)
      if (derivatives.partialDerivatives.empty()) {
        return;
      }
      this->countingLoopCounter = derivatives.countingLoopCounter;
      this->countingLoopBounds = derivatives.countingLoopBounds;
      this->countingLoops = derivatives.countingLoops;
      this->initialStateString = initialRegexp->ToString();
      this->initialState = derivatives.regexNumberMapping.at(this->initialStateString);
      this->computeStateScopes(derivatives);
      this->preprocessCa(derivatives);
      CSA::csaState initialCsaState = {derivatives.caStates[this->initialState]};
      this->stateCounter = 0;
      this->stateIntMap.insert({"", this->stateCounter});
      this->stateCounter++;
      this->stateIntMap.insert({this->initialStateString, this->stateCounter});
      this->stateCounter++;
      std::pair<CSA::csaState, std::string> currentCsaStateToExplore;
      this->csaStates.insert({this->initialStateString, initialCsaState});
      this->csaStatesToExplore.emplace_back(initialCsaState, this->initialStateString);
      this->countingLoopStrings = derivatives.countingLoopStrings;
      this->finalStates.resize(1, {});
      this->finalStates[0] = {{Regexp::Derivatives::True, 0, 0, 0}};
      if (derivatives.finalStates[this->initialState].isSet) {
        this->finalStates.resize(2, {});
        Regexp::Derivatives::finalStateCondition finalCond = derivatives.finalStates[this->initialState];
        this->finalStates[1] = {{finalCond.grd, finalCond.countingLoop, finalCond.counting_min, finalCond.counting_max}};
      }
      this->derivatives_var = derivatives;
      while (!this->csaStatesToExplore.empty()) {
        currentCsaStateToExplore = csaStatesToExplore.back();
        csaStatesToExplore.pop_back();
        alreadyFoundCsaStates.insert(currentCsaStateToExplore.first);
        for (int i = 0; i < bytemapRange; i++) {
          this->getNextStateAndTransitions(currentCsaStateToExplore, i);
        }
      }
      this->printFinalStates();
#else
      if (derivatives.partialDerivatives.empty()) {
        return;
      }
      this->countingLoopCounter = derivatives.countingLoopCounter;
      this->countingLoopBounds = derivatives.countingLoopBounds;
      this->countingLoops = derivatives.countingLoops;
      // Prepare all the information needed to do the on-the-fly determinization
      this->initialStateString = initialRegexp->ToString();
      this->initialState = derivatives.regexNumberMapping.at(this->initialStateString);
      // Scopes are created with the original CA, the transitions that can never be taken are deleted after that
      this->computeStateScopes(derivatives);
      preprocessCa(derivatives);
      CSA::csaState initialCsaState = {derivatives.caStates[this->initialState]};
      // Prepare the numerical representation of already known states
      this->stateCounter = 0;
      // We insert the epsilon state everytime
      this->stateIntMap.insert({"", this->stateCounter});
      this->stateCounter++;
      this->stateIntMap.insert({this->initialStateString, this->stateCounter});
      this->stateCounter++;
      this->intCsaStatePairVector.resize(2);
      this->intCsaStatePairVector[1] = {initialCsaState, this->initialStateString};
      // The epsilon state is always final, it is the state that is reached when the whole regex is matched (and it does
      // not end with the counting loop)
      this->finalStates.resize(1, {});
      this->finalStates[0] = {{Regexp::Derivatives::True, 0, 0, 0}};
      if (derivatives.finalStates[this->initialState].isSet) {
        this->finalStates.resize(2, {});
        Regexp::Derivatives::finalStateCondition finalCond = derivatives.finalStates.at(this->initialState);
        this->finalStates[1] = {{finalCond.grd, finalCond.countingLoop, finalCond.counting_min, finalCond.counting_max}};
      }
      this->derivatives_var = derivatives;
      this->bytemapRange_var = bytemapRange;
#endif
    }

    void
    CSA::getNextStateAndTransitions(const std::pair<CSA::csaState, std::string>& csaStateToExplorePair, int bytemap) {
      Regexp::Derivatives derivatives = this->derivatives_var;
      std::set<unsigned int> currentCaStateScope;
      std::set<unsigned int> targetStateScope;
      std::vector<Regexp::Derivatives::caTransition> outgoingTransitions;
      std::string csaStateString;
      std::string currentCaStateString;
      unsigned int nextCaStateInt;
      CSA::csaState nextCsaState;
      Regexp::Derivatives::caState nextCaState;
      std::string nextCsaStateString;
      std::set<Regexp::Derivatives::counterGuard> counterGuards;
      std::set<finalStateConditionInt> finalStateConditions;
      std::set<CSA::setCounterOperator> csaTransitionOperators;
      std::vector<betterTransition> newBetterTransitions = {};
      std::vector<unsigned int> usedCounters;
      int targetStateInt;
      bool transitionTaken;
      CSA::csaState csaStateToExplore = csaStateToExplorePair.first;
      std::string csaStateToExploreString = csaStateToExplorePair.second;
      transitionTaken = getCounterGuards(csaStateToExplore, bytemap, derivatives, counterGuards, usedCounters);
      std::set<std::set<counterIntGuard>> minterms = {};
      if (!counterGuards.empty() || transitionTaken) {
        minterms = computeMinterms(counterGuards);
      }
      std::set<Regexp::Derivatives::counterGuard> guardSetNegatedLogicalNot;
      Regexp::Derivatives::counterGuard oppositeGuard{};
      std::vector<counterIntGuard> currentGuardsVector;
      // The transitions will be created for each bytemap and each set of guards from the set of minterms
      for (auto const &guardSet: minterms) {
        nextCsaState = {};
        finalStateConditions = {};
        nextCsaStateString = "";
        guardSetNegatedLogicalNot = {};
        csaTransitionOperators = {};
        currentGuardsVector.clear();
        // We create guard set with negated logical_not. It is because we will check satisfiability of minterms and
        // ca transition guards. The conjunction of guards is not satisfiable if there is same guard with negated
        // logical_not. So when we negate the guard set from minterms we can then only check intersection of
        // such set and the set of ca transition guards. If there is at least one same element (i.e. element with
        // opposite logical_not) the conjunction is not satisfiable
        for (auto const &guard: guardSet) {
          oppositeGuard = {guard.grd, guard.countingLoop, guard.countingLoopMin, guard.countingLoopMax, !guard.logical_not};
          guardSetNegatedLogicalNot.insert(oppositeGuard);
          currentGuardsVector.push_back(guard);
        }
        for (auto const &caStateToExplore: csaStateToExplore) {
          // Empty state (i.e. epsilon) can not have any outgoing transitions
          if (derivatives.emptyStateNumber == caStateToExplore.first) {
            continue;
          }
          outgoingTransitions = derivatives.partialDerivatives[caStateToExplore.first];
          currentCaStateScope = caStateToExplore.second;
          for (auto const &transition: outgoingTransitions) {
            // Only transitions with the same bytemap as the one for which the gamma set is computed are relevant
            // Also, as stated in the paper, the counter guards of the original transition operations and the current
            // guard from the gamma set must be satisfiable (specifically their conjunction)
            if ((transition.first.first != 256 && transition.first.first != bytemap) || !checkSatisfiability(guardSetNegatedLogicalNot, transition.second.first)) {
              continue;
            }
            // All target ca states will form a new csa state
            nextCaStateInt = transition.first.second;
            nextCaState = derivatives.caStates[nextCaStateInt];
            nextCsaState.insert(nextCaState);
            if (derivatives.finalStates[nextCaStateInt].isSet) {
              Regexp::Derivatives::finalStateCondition finalCond = derivatives.finalStates[nextCaStateInt];
              finalStateConditions.insert({finalCond.grd, finalCond.countingLoop, finalCond.counting_min, finalCond.counting_max});
            }
            // We need to get scope of the target state of the transition
            // The string representation of target state is on the last position in transition, then we need to get a
            // caState pair based on this string representation. The scope is then second in the caState pair
            targetStateScope = nextCaState.second;
            getCsaTransitionOperator(transition, currentCaStateScope, targetStateScope, csaTransitionOperators);
          }
        }
        if (!nextCsaState.empty()) {
          // If we want to create string representation of next csa state in loops above, we have to check if there is
          // the new ca state in the csa state already (to avoid duplicities in the string). To avoid this check, we create
          // the string here. The set nextCsaState has only unique elements as it is an set
          // For the tests, we need the states sorted as a strings
#if defined(DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS)
          std::set<std::string> nextCsaStateStrings;
          for (auto const &caState: nextCsaState) {
            nextCsaStateStrings.insert(derivatives.numberRegexMapping.at(caState.first));
          }
          for (auto const &caState: nextCsaStateStrings) {
            nextCsaStateString += caState + "; ";
          }
          // Delete trailing semicolon
          nextCsaStateString = nextCsaStateString.substr(0, nextCsaStateString.length()-2);
#else
          for (auto const &caState: nextCsaState) {
            nextCsaStateString += std::to_string(caState.first) + ";";
          }
          // Delete trailing semicolon
          nextCsaStateString = nextCsaStateString.substr(0, nextCsaStateString.length()-1);
#endif
          // Get the numerical representation of the state
          if (this->stateIntMap.count(nextCsaStateString) == 1) {
            targetStateInt = this->stateIntMap.at(nextCsaStateString);
          } else {
            targetStateInt = this->stateCounter;
            this->stateIntMap.insert({nextCsaStateString, this->stateCounter});
            this->stateCounter++;
          }
          // Save the target state so it can be used later, when the csa state pair is used to compute its outgoing
          // transitions
          if (this->intCsaStatePairVector.size() <= (unsigned long)targetStateInt) {
            csaState emptyState = {};
            this->intCsaStatePairVector.resize(targetStateInt + 1, {emptyState, ""});
          }
          this->intCsaStatePairVector[targetStateInt] = {nextCsaState, nextCsaStateString};
#if defined(SCOPETESTS) || defined(GAMMASETTESTS) || defined (DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS) || defined(DETERMINISTICBENCHMARKS)
          this->csaStates.insert({nextCsaStateString, nextCsaState});
#endif
          if (!finalStateConditions.empty()) {
            if (this->finalStates.size() <= (unsigned long)targetStateInt) {
              this->finalStates.resize(targetStateInt+1, {{Regexp::Derivatives::False, 0, -1, -1}});
            }
            this->finalStates[targetStateInt] = finalStateConditions;
          }
#if defined(SCOPETESTS) || defined(GAMMASETTESTS) || defined (DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS) || defined(DETERMINISTICBENCHMARKS)
          if (this->alreadyFoundCsaStates.count(nextCsaState) == 0) {
            this->csaStatesToExplore.emplace_back(nextCsaState, nextCsaStateString);
          }
          alreadyFoundCsaStates.insert(nextCsaState);
#endif
#if defined(DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS)
          newBetterTransitions.emplace_back(csaTransitionOperators, guardSet, targetStateInt);
#else
          // If there are no counters, the guard will be TRUE and it is not necessary to compute the index
          if (usedCounters.empty()) {
            newBetterTransitions.emplace_back(csaTransitionOperators, targetStateInt);
          } else {
            std::vector<unsigned long> transitionIndexes;
            // Get the indexes of the transition, resize the vector to be able to insert even the biggest computed index
            transitionIndexes = computeGuardIndexes(currentGuardsVector);
            betterTransition newTransition = {csaTransitionOperators, targetStateInt};
            if (newBetterTransitions.size() < transitionIndexes.back()+1) {
              try {
                newBetterTransitions.resize(transitionIndexes.back() + 1, {{{CSA::S_ID, 0, -1, -1}}, -1});
              } catch (const std::bad_alloc &e) {
                std::cerr << "NOT ENOUGH MEMORY" << std::endl;
                exit(EXIT_FAILURE);
              }
            }
            // Save the transition on computed indexes
            for (auto const &vectorIndex: transitionIndexes) {
              newBetterTransitions[vectorIndex] = newTransition;
            }
          }
#endif
        }
      }
      if (!newBetterTransitions.empty() || csaStateToExploreString.empty()) {
#if defined (DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS)
        std::vector<betterTransition> defaultTransition = {{{{CSA::S_ID, 0, -1, -1}}, {{Regexp::Derivatives::True, 0, -1, -1}}, -1}};
        std::vector<unsigned int> defaultUsedCounters;
        std::pair<std::vector<unsigned int>, std::vector<betterTransition>> defaultVectorArrayValue = {defaultUsedCounters, defaultTransition};
#else
        std::vector<betterTransition> defaultTransition = {{{{CSA::S_ID, 0, -1, -1}}, -1}};
        std::vector<unsigned int> defaultUsedCounters;
        std::pair<std::vector<unsigned int>, std::vector<betterTransition>> defaultVectorArrayValue = {defaultUsedCounters, defaultTransition};
#endif
        // Save the outgoing transition to appropriate source state and bytemap combination, resize them if needed
        int sourceStateInt = this->stateIntMap.at(csaStateToExploreString);
        if (this->betterCountingSetAutomaton.size() <= (unsigned long)sourceStateInt) {
          this->betterCountingSetAutomaton.resize(sourceStateInt+1, {defaultVectorArrayValue});
        }
        vectorArrayType vectorArray = this->betterCountingSetAutomaton[sourceStateInt];
        if (vectorArray.size() <= (unsigned long)bytemap) {
          vectorArray.resize(bytemap+1, defaultVectorArrayValue);
        }
        vectorArray[bytemap] = {usedCounters, newBetterTransitions};
        this->betterCountingSetAutomaton[sourceStateInt] = vectorArray;
      }
    }

    bool CSA::getCounterGuards(const CSA::csaState& csaStateToExplore, int bytemap, Regexp::Derivatives derivatives, std::set<Regexp::Derivatives::counterGuard> &counterGuards, std::vector<unsigned int> &usedCounters) {
      std::set<unsigned int> currentCaStateScope;
      std::vector<Regexp::Derivatives::caTransition> outgoingTransitions;
      std::string currentCaStateString;
      std::set<unsigned int> usedCountersSet;
      // We need to decide whether any transition was taken for specified bytemap or not
      // When there is no counter guard in scope but transition was taken, the minterms will contain True
      // When there is no counter guard in scope and transition was not taken, the minterms will be empty as there are
      // no transitions to take
      bool transitionTaken = false;
      // All ca states from the current csa state must be explored
      // All counter guards of transitions from all ca states must be gathered in order to compute minterms (i.e gamma set)
      for (auto const &caStateToExplore: csaStateToExplore) {
        // Empty state (i.e. epsilon) can not have any outgoing transitions
        if (caStateToExplore.first == derivatives.emptyStateNumber) {
          continue;
        }
        outgoingTransitions = derivatives.partialDerivatives[caStateToExplore.first];
        currentCaStateScope = caStateToExplore.second;
        for (auto const &transition: outgoingTransitions) {
          // Only transitions with the same bytemap as the one for which the gamma set is computed are relevant
          if (transition.first.first != bytemap && transition.first.first != 256) {
            continue;
          }
          transitionTaken = true;
          // Explore all operators on the transition
          for (auto const &transitionOperator: transition.second.second) {
            // Only operators for counters in scope are relevant
            if (currentCaStateScope.count(transitionOperator.countingLoop) == 1) {
              switch (transitionOperator.op) {
                case Regexp::Derivatives::ID:
                  // If there is some other guard, the True guard does not have to be inserted, since it is always satisfiable and only
                  // the other guards will be tested
                  if (counterGuards.empty()) {
                    counterGuards.insert({Regexp::Derivatives::True, transitionOperator.countingLoop, 0, 0, false});
                  }
                  counterGuards.insert({Regexp::Derivatives::True, transitionOperator.countingLoop, 0, 0, false});
                  break;
                case Regexp::Derivatives::INCR:
                  // The True guard is not needed when we insert some other guard
                  if (counterGuards.size() == 1 && counterGuards.begin()->grd == Regexp::Derivatives::True) {
                    counterGuards.clear();
                  }
                  counterGuards.insert({Regexp::Derivatives::CanIncr, transitionOperator.countingLoop, transitionOperator.counting_min, transitionOperator.counting_max, false});
                  usedCountersSet.insert(transitionOperator.countingLoop);
                  break;
                case Regexp::Derivatives::EXIT:
                case Regexp::Derivatives::EXIT1:
                  // The True guard is not needed when we insert some other guard
                  if (counterGuards.size() == 1 && counterGuards.begin()->grd == Regexp::Derivatives::True) {
                    counterGuards.clear();
                  }
                  usedCountersSet.insert(transitionOperator.countingLoop);
                  counterGuards.insert({Regexp::Derivatives::CanExit, transitionOperator.countingLoop, transitionOperator.counting_min, transitionOperator.counting_max,false});
                  break;
              }
            }
          }
        }
      }
      for (auto const &counter: usedCountersSet) {
        usedCounters.push_back(counter);
      }
      return transitionTaken;
    }

    void CSA::computeStateScopes(Regexp::Derivatives &derivatives) {
      // Nothing to compute for an empty automaton
      if (derivatives.partialDerivatives.empty()) {
        return;
      }
      std::unordered_set<unsigned int>::iterator foundSetInsert;
      std::set<unsigned int>::iterator foundSetUpdate;
      std::vector<unsigned int> statesToExplore = {this->initialState};
      Regexp::Derivatives::caState currentState;
      unsigned int nextState;
      // The only possibility of a state not being in CA is when it is the empty state (epsilon), such a state will
      // be skipped since it cannot have any outgoing transitions. So it is added to the explored state immediately
      std::unordered_set<unsigned int> exploredStates {derivatives.emptyStateNumber};
      std::set<unsigned int> nextStateScope;
      std::set<unsigned int> currentStateScope;
      std::set<unsigned int> mergedScopeSet;
      std::set<unsigned int> setIntersectionResult;
      std::set<unsigned int> unusedCounters;
      std::unordered_set<unsigned int> usedCounters;
      std::pair<int, int> counterBounds;
      // Only reachable states will be explored
      while(!statesToExplore.empty()) {
        currentState = derivatives.caStates[statesToExplore.back()];
        exploredStates.insert(statesToExplore.back());
        statesToExplore.pop_back();
        currentStateScope = currentState.second;
        std::vector<Regexp::Derivatives::caTransition> currentStateOutgoingTransitions = derivatives.partialDerivatives[currentState.first];
        // Explore all outgoing transitions of the current states
        for (auto &transition: currentStateOutgoingTransitions) {
          usedCounters.clear();
          std::list<Regexp::Derivatives::counterOperator> counterOperatorList = transition.second.second;
          nextState = transition.first.second;
          nextStateScope = derivatives.caStates[transition.first.second].second;
          foundSetInsert = exploredStates.find(nextState);
          // All newly discovered state must be also explored
          if (foundSetInsert == exploredStates.end()) {
            statesToExplore.push_back(transition.first.second);
          }
          for (auto op = counterOperatorList.begin(); op != counterOperatorList.end(); ) {
            switch (op->op) {
              case Regexp::Derivatives::ID:
                // There must be implicit ID operation for all counters, that are not used with another operation
                // But throughout the derivatives computation we do not have information about all counters and
                // their usage, so with the ID operation we insert empty counting loop. So we delete the ID operator
                // with empty counting loop and we add a new ID operator with for each counting loop that is not used
                // on the transition (done after looping through all operators to get all unused operators)
                if (op->countingLoop == 0) {
                  op = counterOperatorList.erase(op);
                } else {
                  usedCounters.insert(op->countingLoop);
                  op++;
                }
                break;
              // In terms of scope, the INCR and EXIT1 operations are the same - the corresponding counter is entering
              // the scope
              case Regexp::Derivatives::INCR:
              case Regexp::Derivatives::EXIT1:
                // All used counters must be remembered to correctly determine ID operations and scope
                usedCounters.insert(op->countingLoop);
                foundSetUpdate = nextStateScope.find(op->countingLoop);
                // If the counter is not in scope of the next state, it must be added
                if (foundSetUpdate == nextStateScope.end()) {
                  nextStateScope.insert(op->countingLoop);
                  derivatives.caStates[transition.first.second].second = nextStateScope;
                  statesToExplore.push_back(transition.first.second);
                } else if (foundSetInsert == exploredStates.end()) {
                  // The state must be explored if it was not explored before
                  statesToExplore.push_back(transition.first.second);
                }
                op++;
                break;
              case Regexp::Derivatives::EXIT:
                // All used counters must be remembered to correctly determine ID operations and scope
                usedCounters.insert(op->countingLoop);
                foundSetUpdate = nextStateScope.find(op->countingLoop);
                // When the EXIT operation is used, the scope ends
                if (foundSetUpdate != nextStateScope.end()) {
                  nextStateScope.erase(op->countingLoop);
                  derivatives.caStates[transition.first.second].second = nextStateScope;
                  statesToExplore.push_back(transition.first.second);
                } else if (foundSetInsert == exploredStates.end()) {
                  statesToExplore.push_back(transition.first.second);
                }
                op++;
                break;
            }
          }
          unusedCounters.clear();
          // Get all counters of the whole regex that are not used in outgoing transitions of current state
          unordered_set_difference(this->countingLoops, usedCounters, unusedCounters);

          // We add implicit ID operation for each counter that is not used
          // We will also add counter guards for such operations
          if (!unusedCounters.empty()) {
            // There can be a guard of the ID operation with empty counting loop inserted in derivative construction
            // We delete such guard as we will add guards with counting loops
            std::set<Regexp::Derivatives::counterGuard> guardSet = transition.second.first;
            guardSet.erase({Regexp::Derivatives::True, 0, false});
            Regexp::Derivatives::counterOperator newOperator{};
            for (auto const &unusedCounter: unusedCounters) {
              counterBounds = derivatives.countingLoopBounds[unusedCounter];
              newOperator = {Regexp::Derivatives::ID, unusedCounter, counterBounds.first, counterBounds.second};
              counterOperatorList.push_back(newOperator);
              guardSet.insert(Regexp::Derivatives::getGuardForOperator(newOperator));
            }
            transition.second.second = counterOperatorList;
            transition.second.first = guardSet;
          }

          setIntersectionResult.clear();
          // Then all not used counters that are also in scope of the current state will spread the scope of itself
          // to the next state
          std::set_intersection(unusedCounters.begin(), unusedCounters.end(),
                                currentStateScope.begin(), currentStateScope.end(),
                                std::inserter(setIntersectionResult, setIntersectionResult.begin()));
          if (!setIntersectionResult.empty()) {
            std::set_union(currentStateScope.begin(), currentStateScope.end(),
                           nextStateScope.begin(), nextStateScope.end(),
                           std::inserter(mergedScopeSet, mergedScopeSet.begin()));
            derivatives.caStates[transition.first.second].second = mergedScopeSet;
            mergedScopeSet.clear();
            statesToExplore.push_back(transition.first.second);
          }
        }
        derivatives.partialDerivatives[currentState.first] = currentStateOutgoingTransitions;
      }
    }

    std::set<std::set<CSA::counterIntGuard>> CSA::computeMinterms(const std::set<Regexp::Derivatives::counterGuard>& counterGuards) {
      if (counterGuards.empty()) {
        return {{{Regexp::Derivatives::True, 0, 0, 0, false}}};
      }
      // In order to use random access
      std::vector<Regexp::Derivatives::counterGuard> counterGuardsVec(counterGuards.begin(), counterGuards.end());
      unsigned long vecIndex = 0;
      unsigned long vecMaxIndex = counterGuardsVec.size()-1;
      std::set<std::set<counterIntGuard>> currentLeafNodes;
      std::set<std::set<counterIntGuard>> tmpLeafNodes;
      std::set<counterIntGuard> tmpLeafNode;
      counterIntGuard currentGuard = {counterGuardsVec[vecIndex].grd, counterGuardsVec[vecIndex].countingLoop, counterGuardsVec[vecIndex].counting_min, counterGuardsVec[vecIndex].counting_max, counterGuardsVec[vecIndex].logical_not};
      counterIntGuard currentGuardOppositeNot = {currentGuard.grd, currentGuard.countingLoop, currentGuard.countingLoopMin, currentGuard.countingLoopMax, !currentGuard.logical_not};
      // The first counter guard (and its negated version) are the first leaves
      currentLeafNodes.insert({currentGuard});
      if (currentGuardOppositeNot.grd != Regexp::Derivatives::True) {
        currentLeafNodes.insert({currentGuardOppositeNot});
      }
      if (vecIndex == vecMaxIndex) {
        return currentLeafNodes;
      }
      vecIndex++;
      // All guards must be processed
      while (vecIndex <= vecMaxIndex) {
        currentGuard = {counterGuardsVec[vecIndex].grd, counterGuardsVec[vecIndex].countingLoop, counterGuardsVec[vecIndex].counting_min, counterGuardsVec[vecIndex].counting_max, counterGuardsVec[vecIndex].logical_not};
        currentGuardOppositeNot = {currentGuard.grd, currentGuard.countingLoop, currentGuard.countingLoopMin, currentGuard.countingLoopMax, !currentGuard.logical_not};
        tmpLeafNodes.clear();
        // Each node will be extended by the new guard
        for (auto const &leaf: currentLeafNodes) {
          tmpLeafNode = leaf;
          // All counter guards has initially logical_not set to false, therefore, as there is no possibility of the
          // guards become unsatisfiable by adding guard with logical_not set to false, the current guard will always
          // be added
          tmpLeafNode.insert(currentGuard);
          tmpLeafNodes.insert(tmpLeafNode);
          tmpLeafNode = leaf;
          // There are only two situations when the guard can not be satisfied -> "not CanIncr and not CanExit" and "not True"
          // When it is satisfiable it will be further extended. If it is not, it is one of the final leaf nodes
          if (currentGuardOppositeNot.grd == Regexp::Derivatives::CanIncr && currentGuardOppositeNot.logical_not) {
            if (leaf.count({Regexp::Derivatives::CanExit, currentGuardOppositeNot.countingLoop, currentGuardOppositeNot.countingLoopMin, currentGuardOppositeNot.countingLoopMax, true}) == 0) {
              tmpLeafNode.insert(currentGuardOppositeNot);
              tmpLeafNodes.insert(tmpLeafNode);
            }
          } else if (currentGuardOppositeNot.grd == Regexp::Derivatives::CanExit && currentGuardOppositeNot.logical_not) {
            if (leaf.count({Regexp::Derivatives::CanIncr, currentGuardOppositeNot.countingLoop, currentGuardOppositeNot.countingLoopMin, currentGuardOppositeNot.countingLoopMax, true}) == 0) {
              tmpLeafNode.insert(currentGuardOppositeNot);
              tmpLeafNodes.insert(tmpLeafNode);
            }
          } else {
            // "not true" can not be satisfied
            if (currentGuardOppositeNot.grd != Regexp::Derivatives::True) {
              tmpLeafNode.insert(currentGuard);
              tmpLeafNodes.insert(tmpLeafNode);
            }
          }
        }
        vecIndex++;
        currentLeafNodes = tmpLeafNodes;
      }
      return currentLeafNodes;
    }

    void CSA::getCsaTransitionOperator(const Regexp::Derivatives::caTransition& transition, const std::set<unsigned int>& currentCaStateScope, const std::set<unsigned int>& targetCaStateScope, std::set<CSA::setCounterOperator> &csaTransitionOperators) {
      for (auto const &counterOperator: transition.second.second) {
        // The counting set operator is obtained only when the loop is in scope of the target state
        if (targetCaStateScope.count(counterOperator.countingLoop) == 0) {
          continue;
        }
        // Implemented according to the equations from the paper
        if (counterOperator.op == Regexp::Derivatives::ID) {
          if (currentCaStateScope.count(counterOperator.countingLoop) == 1) {
            csaTransitionOperators.insert({CSA::S_ID, counterOperator.countingLoop, counterOperator.counting_min, counterOperator.counting_max});
          } else {
            csaTransitionOperators.insert({CSA::S_RST, counterOperator.countingLoop, counterOperator.counting_min, counterOperator.counting_max});
          }
        } else if (counterOperator.op == Regexp::Derivatives::INCR) {
          if (currentCaStateScope.count(counterOperator.countingLoop) == 1) {
            csaTransitionOperators.insert({CSA::S_INCR, counterOperator.countingLoop, counterOperator.counting_min, counterOperator.counting_max});
          } else {
            csaTransitionOperators.insert({CSA::S_RST1, counterOperator.countingLoop, counterOperator.counting_min, counterOperator.counting_max});
          }
        } else if (counterOperator.op == Regexp::Derivatives::EXIT) {
          csaTransitionOperators.insert({CSA::S_RST, counterOperator.countingLoop, counterOperator.counting_min, counterOperator.counting_max});
        } else if (counterOperator.op == Regexp::Derivatives::EXIT1) {
          csaTransitionOperators.insert({CSA::S_RST1, counterOperator.countingLoop, counterOperator.counting_min, counterOperator.counting_max});
        }
      }
    }

    bool CSA::checkSatisfiability(const std::set<Regexp::Derivatives::counterGuard>& firstSet,
                                  const std::set<Regexp::Derivatives::counterGuard>& secondSet) {
      // The conjunction of two sets of guards is not satisfiable if there is at least one guard with
      // opposite logical_not (i.e., CanIncr land not CanIncr). One of the sets must contain guards, which has negated
      // logical_not. Then, the originally opposite elements will be the same, and we can just test if there is at least
      // one same element in both of the sets (i.e., we do intersection and check if the result is nonempty)
      std::set<Regexp::Derivatives::counterGuard> setIntersectionResult;
      std::set_intersection(firstSet.begin(), firstSet.end(),
                            secondSet.begin(), secondSet.end(),
                            std::inserter(setIntersectionResult, setIntersectionResult.begin()));
      return setIntersectionResult.empty();
    }

    void
    CSA::unordered_set_difference(const std::unordered_set<unsigned int>& first, const std::unordered_set<unsigned int>& second,
                                          std::set<unsigned int> &result) {
      for (auto const &firstSetElem: first) {
        if (second.count(firstSetElem) == 0){
          result.insert(firstSetElem);
        }
      }
    }

    // Gets the minimum value of the counter, obtained as offset minus the last (the biggest) element of the queue
    int CSA::counterMinimumValue(unsigned int counterInt) {
      return this->countingSetMemory[counterInt].first - this->countingSetMemory[counterInt].second.back();
    }

    // Gets the maximum value of the counter, obtained as offset minus the first (the smallest) element of the queue
    int CSA::counterMaximumValue(unsigned int counterInt) {
      return this->countingSetMemory[counterInt].first - this->countingSetMemory[counterInt].second.front();
    }

    // Insert zero to the counter memory, used only if there was INCR or ID operation as this method extends the set
    // (i.e., makes the union of the result of the two operations)
    void CSA::resetCounter(unsigned int counterInt) {
      std::pair<int, std::deque<int>> countingSetValue = this->countingSetMemory[counterInt];
      // We do not want duplicities, so if there is already zero, do not add it again
      if (this->counterMinimumValue(counterInt) > 0) {
        countingSetValue.second.push_back(countingSetValue.first);
      }
      this->countingSetMemory[counterInt] = countingSetValue;
    }

    // Insert one to the counter memory, used only if there was INCR or ID operation as this method extends the set
    // (i.e., makes the union of the result of the two operations)
    void CSA::resetCounterOne(unsigned int counterInt) {
      std::pair<int, std::deque<int>> countingSetValue = this->countingSetMemory[counterInt];
      // If the minimum value is zero, we must explore the second minimum value as the structure holds strictly
      // increasing integers, so the new value (one) can not be after the current last value (zero)
      if (this->counterMinimumValue(counterInt) == 0) {
        int lastCounterMemoryValue = countingSetValue.second.back();
        countingSetValue.second.pop_back();
        // SO we do not get -1 in the queue
        if (lastCounterMemoryValue == 0 && countingSetValue.first == 0) {
          countingSetValue.first++;
          countingSetValue.second.push_back(countingSetValue.first-1);
          this->countingSetMemory[counterInt] = countingSetValue;
          return;
        }
        // if the zero was the only value, we just add the new value and then return the original (zero)
        if (countingSetValue.second.empty()) {
          countingSetValue.second.push_back(countingSetValue.first-1);
          countingSetValue.second.push_back(lastCounterMemoryValue);
          this->countingSetMemory[counterInt] = countingSetValue;
          return;
        }
        int secondMinimumCounterValue = countingSetValue.first - countingSetValue.second.back();
        // if the second minimum value is already one, we do not insert in twice and we return the original minimum value
        // which is zero
        if (secondMinimumCounterValue == 1) {
          countingSetValue.second.push_back(lastCounterMemoryValue);
        } else {
          // However, if the second minimum value is not one, we add the new value (one) and then return the original
          // minimum value which is zero
          countingSetValue.second.push_back(countingSetValue.first-1);
          countingSetValue.second.push_back(lastCounterMemoryValue);
        }
      } else if (this->counterMinimumValue(counterInt) > 1) {
        // if the minimum value is greater that one, we just add the new value (one)
        countingSetValue.second.push_back(countingSetValue.first - 1);
      }
      this->countingSetMemory[counterInt] = countingSetValue;
    }

    // Increment counter, i.e., increment offset, delete the biggest value if it is out of counter bounds
    void CSA::incrementCounter(unsigned int counterInt, int counterMax) {
      std::pair<int, std::deque<int>> countingSetValue = this->countingSetMemory[counterInt];
      countingSetValue.first += 1;
      // counterMax == -1 means unlimited upper bound
      if (counterMax == -1) {
        this->countingSetMemory[counterInt] = countingSetValue;
        return;
      }
      // if the maximum value is above the counting loop bound, we delete it as it is useless now
      if (countingSetValue.first - countingSetValue.second.front() > counterMax) {
        countingSetValue.second.pop_front();
      }
      this->countingSetMemory[counterInt] = countingSetValue;
    }

    // If the counter is incremented and there was also the ID operation, the resulting set must contain value of both
    // of the results of the operations
    void CSA::incrementCounterAfterID(unsigned int counterInt, int counterMax) {
      std::pair<int, std::deque<int>> countingSetValue = this->countingSetMemory[counterInt];
      unsigned dequeSize = countingSetValue.second.size();
      std::set<int> allValues;
      for (unsigned i = 0; i < dequeSize; i++) {
        allValues.insert(countingSetValue.second[i]);
        allValues.insert(countingSetValue.second[i]+1);
      }
      countingSetValue.second.clear();
      for (auto const &value: allValues) {
        countingSetValue.second.push_back(value);
      }
      // if the maximum value is above the counting loop bound, we delete it as it is useless now
      if (countingSetValue.first - countingSetValue.second.front() > counterMax) {
        countingSetValue.second.pop_front();
      }
      this->countingSetMemory[counterInt] = countingSetValue;
    }

    // The methods initializes counter memory for all counters, inserting zero to them
    void CSA::initCounterMemory() {
      std::pair<int, std::deque<int>> initialCounterMemory = {0, {0}};
      this->countingSetMemory.clear();
      this->countingSetMemory.resize(this->countingLoopCounter, initialCounterMemory);
    }


    // The method updates the counter memory with the operations
    void CSA::updateMemory(const std::set<CSA::setCounterOperator>& operations) {
      bool wasIDOp = false;
      bool wasINCROp = false;
      for (auto const &ops: operations) {
        // The ID operation does not change the values of the counter
        if (ops.op == CSA::S_ID) {
          wasIDOp = true;
        }
        if (ops.op == CSA::S_INCR) {
          // If there was the ID operation the results of these two operations must be merged
          if (wasIDOp) {
            this->incrementCounterAfterID(ops.countingLoop, ops.counting_max);
          } else {
            this->incrementCounter(ops.countingLoop, ops.counting_max);
          }
          wasINCROp = true;
        }
        if (ops.op == CSA::S_RST1) {
          // If there was transition with INCR or ID operation, the reset operation will add zero to the already existing set
          // Otherwise, the reset operation is the only operation and it will reset the whole memory of the counter
          // so there will be only one element
          if (wasIDOp || wasINCROp) {
            this->resetCounterOne(ops.countingLoop);
          } else {
            this->countingSetMemory[ops.countingLoop] = {1, {0}};
          }
        }
        if (ops.op == CSA::S_RST) {
          // If there was transition with INCR or ID operation, the reset operation will add one to the already existing set
          // Otherwise, the reset1 operation is the only operation and it will reset the whole memory of the counter
          // so there will be only one element (with the value of one)
          if (wasIDOp || wasINCROp) {
            this->resetCounter(ops.countingLoop);
          } else {
            this->countingSetMemory[ops.countingLoop] = {0, {0}};
          }
        }
      }
    }

    // Matching interface,
    bool CSA::match(const StringPiece &text, const uint8_t* bytemapArray) {
#if defined(DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS) || defined(DETERMINISTICBENCHMARKS)
      return true;
#else
      this->initCounterMemory();
      int currentState = this->stateIntMap.at(this->initialStateString);
      std::pair<std::vector<int>, std::vector<betterTransition>> tmpTransitionPair;
      int currentBytemap;
      bool canExit;
      unsigned long vectorIndex;
      // Traverse the input text
      for (char const &character: text) {
        std::vector<CSA::betterTransition> transitionVector;
        std::vector<unsigned int> usedCounters;
        // Get the bytemap class of the current character
        currentBytemap = bytemapArray[(unsigned char)character];
        // Resize the vector holding information about the states being computed if needed, insert false as the default
        // value
        if (this->stateBytemapComputedVector.size() <= (unsigned long)currentState) {
          this->stateBytemapComputedVector.resize(currentState+1, std::vector<bool>(this->bytemapRange_var, false));
        }
        // If the state was not computed before, call the determinization function to process the current state and
        // get the outgoing transitions
        if (!this->stateBytemapComputedVector[currentState][currentBytemap]) {
          this->getNextStateAndTransitions(this->intCsaStatePairVector[currentState], currentBytemap);
          this->stateBytemapComputedVector[currentState][currentBytemap] = true;
        }
        // If no transitions were computed the match fails
        if (this->betterCountingSetAutomaton.size() <= (unsigned long)currentState) {
          return false;
        }
        if ((unsigned long)currentBytemap >= this->betterCountingSetAutomaton[currentState].size()) {
          return false;
        }
        // Get the used counters and transitions vector, if the transitions vector is empty, there are no transitions to
        // take and the match fails
        usedCounters = this->betterCountingSetAutomaton[currentState][currentBytemap].first;
        transitionVector = this->betterCountingSetAutomaton[currentState][currentBytemap].second;
        if (transitionVector.empty()) {
          return false;
        }
        // Compute the index of the appropriate outgoing transition based on the current state of the counter memory
        vectorIndex = getVectorIndexFromUsedCounters(usedCounters);
        // If there is a valid transition on the computed index, update the memory by all its operations
        if ((unsigned long)vectorIndex <= transitionVector.size()-1) {
          currentState = transitionVector[vectorIndex].second;
          if (currentState == -1) {
            return false;
          }
          this->updateMemory(transitionVector[vectorIndex].first);
        } else {
          // outgoing transition not found
          return false;
        }
      }
      // The whole text was traversed, check if the last state is alsi the final state, if not, the match fails
      if ((unsigned long)currentState >= this->finalStates.size()) {
        return false;
      }
      std::set<finalStateConditionInt> finalConditions = this->finalStates[currentState];
      // The False guard is inserted as default value on not used indexes, if the guard is False, the state is not final
      if (finalConditions.size() == 1 && finalConditions.begin()->grd == Regexp::Derivatives::False) {
        return false;
      }
      // The final state must also satisfy at least on of its final state conditions, otherwise the match fails
      for (auto const &condition: finalConditions) {
        if (condition.grd == Regexp::Derivatives::CanExit) {
          canExit = this->counterMaximumValue(condition.countingLoop) >= condition.counting_min;
          if (canExit) {
            return true;
          }
        }
        // If the condition is True, the state is final without checking anything
        if (condition.grd == Regexp::Derivatives::True) {
          return true;
        }
      }
      return false;
#endif
    }

    void CSA::preprocessCa(Regexp::Derivatives &derivatives) {
      // If there is a transition from the state which is not in scope with EXIT or EXIT 1 operation for counter with
      // lower bound > 0, it can not be taken, since the state is out of scope of the counter, it will be zero everytime
      // Such transitions will be deleted, since they can never be taken
      std::set<unsigned int> currentStateScope;
      Regexp::Derivatives::caState currentState;
      std::vector<Regexp::Derivatives::caTransition> updatedTransitions;
      bool useTransition = true;
      for (unsigned int i = 0; i < derivatives.partialDerivatives.size(); i++) {
        if (!derivatives.existingPartialDerivatives[i]) {
          continue;
        }
        currentState = derivatives.caStates[i];
        currentStateScope = currentState.second;
        std::vector<Regexp::Derivatives::caTransition> currentStateOutgoingTransitions = derivatives.partialDerivatives[i];
        // Explore all outgoing transitions of the current states
        updatedTransitions.clear();
        for (auto const &transition: currentStateOutgoingTransitions) {
          useTransition = true;
          std::list<Regexp::Derivatives::counterOperator> counterOperatorList = transition.second.second;
          for (auto const &op: counterOperatorList) {
            switch (op.op) {
              case Regexp::Derivatives::ID:
              case Regexp::Derivatives::INCR:
                break;
              case Regexp::Derivatives::EXIT1:
              case Regexp::Derivatives::EXIT:
                if (op.counting_min > 0) {
                  if (currentStateScope.count(op.countingLoop) == 0) {
                    useTransition = false;
                    break;
                  }
                }
                break;
            }
          }
          if (useTransition) {
            updatedTransitions.push_back(transition);
          }
        }
        derivatives.partialDerivatives[i] = updatedTransitions;
      }
    }

    std::vector<unsigned long> CSA::computeGuardIndexes(std::vector<counterIntGuard> guards) {
      std::vector<unsigned long> result = {};
      std::pair<unsigned long, unsigned long> indexes;
      unsigned long firstGuardInt;
      unsigned long secondGuardInt;
      unsigned long guardsSize = guards.size();
      int countingLoopNum = 0;
      // Traverse all the used counters, their will be identified by the iteration variable
      for (unsigned long guardsIndex = 0; guardsIndex < guardsSize;) {
        // If there are two consecutive guards of the same counting loop, the must be processed together as
        // the counting condition must satisfy both of them
        if (guardsIndex+1 < guardsSize && guards[guardsIndex].countingLoop == guards[guardsIndex+1].countingLoop) {
          firstGuardInt = getGuardNumber(guards[guardsIndex], guards[guardsIndex+1], true).first;
          guardsIndex++;
          updateIndexes(result, countingLoopNum, firstGuardInt);
        } else {
          indexes = getGuardNumber(guards[guardsIndex]);
          firstGuardInt = indexes.first;
          secondGuardInt = indexes.second;
          updateIndexes(result, countingLoopNum, firstGuardInt, secondGuardInt);
        }
        guardsIndex++;
        countingLoopNum++;
      }
      return result;
    }

    std::pair<unsigned long, unsigned long> CSA::getGuardNumber(counterIntGuard firstGuard, counterIntGuard secondGuard, bool twoGuards) {
      unsigned long firstResult = 0;
      unsigned long secondResult = 0;
      if (firstGuard.grd == Regexp::Derivatives::CanIncr) {
        if (firstGuard.logical_not) {
          // not CanIncr -> HIGH
          firstResult = countingConditions::HIGH;
        } else {
          // CanIncr -> LOW and MIDDLE
          if (twoGuards) {
            // If the first is CanIncr the second has to be CanExit
            // CanIncr in this situation can be LOW or MIDDLE
            if (secondGuard.logical_not) {
              // not CanExit -> LOW, combination with the first yields LOW
              firstResult = countingConditions::LOW;
            } else {
              // CanExit -> MIDDLE and HIGH, combination with the first yields MIDDLE
              firstResult = countingConditions::MIDDLE;
            }
          } else {
            firstResult = countingConditions::LOW;
            secondResult = countingConditions::MIDDLE;
          }
        }
      } else if (firstGuard.grd == Regexp::Derivatives::CanExit) {
        if (firstGuard.logical_not) {
          // not CanExit -> LOW
          firstResult = countingConditions::LOW;
        } else {
          // CanExit -> MIDDLE and HIGH
          if (twoGuards) {
            // If the first is CanExit the second has to be CanIncr
            // CanExit in this situation can be MIDDLE or HIGH
            if (secondGuard.logical_not) {
              // not CanIncr -> HIGH, combination with the first yields HIGH
              firstResult = countingConditions::HIGH;
            } else {
              // CanIncr -> LOW and MIDDLE, combination with the first yields MIDDLE
              firstResult = countingConditions::MIDDLE;
            }
          } else {
            firstResult = countingConditions::MIDDLE;
            secondResult = countingConditions::HIGH;
          }
        }
      }
      return {firstResult, secondResult};
    }

    void CSA::updateIndexes(std::vector<unsigned long> &result, int countingLoop, unsigned long firstGuardInt, unsigned long secondGuardInt) {
      std::vector<unsigned long> tmpIndexes = {};
      // If there is no index, insert the first one
      if (result.empty()) {
        // The bit shift shifts the counting condition to the position that will represent the counting loop
        result.push_back(firstGuardInt << countingLoop*2);
        if (secondGuardInt != 0) {
          result.push_back(secondGuardInt << countingLoop*2);
        }
      } else {
        // If there already are indexes, insert the information about the current counting loop to all of them
        for (auto const &index: result) {
          tmpIndexes.push_back(index | firstGuardInt << countingLoop*2);
          if (secondGuardInt != 0) {
            tmpIndexes.push_back(index | secondGuardInt << countingLoop*2);
          }
        }
      }
      if (!tmpIndexes.empty()) {
        result = tmpIndexes;
      }
    }

    unsigned long CSA::getVectorIndexFromUsedCounters(const std::vector<unsigned int>& usedCounters) {
      unsigned long index = 0;
      int counterNum = 0;
      // Get the appropriate counting condition for each of the used counters and saves them on appropriate positions
      // for the individual counters in the index
      for (auto const &counter: usedCounters) {
        if (this->counterMaximumValue(counter) < this->countingLoopBounds[counter].first) {
          index |= countingConditions::LOW << counterNum*2;
        } else if (this->countingSetMemory[counter].second.size() == 1 && this->counterMaximumValue(counter) == this->countingLoopBounds[counter].second) {
          index |= countingConditions::HIGH << counterNum*2;
        } else {
          index |= countingConditions::MIDDLE << counterNum*2;
        }
        counterNum++;
      }
      return index;
    }


    // Just for testing

    //Extended getCounterGuards to create gamma set
    void CSA::updateGammaSet(const CSA::csaState& csaStateToExplore, int bytemap, Regexp::Derivatives derivatives) {
      std::set<unsigned int> currentCaStateScope;
      std::vector<Regexp::Derivatives::caTransition> outgoingTransitions;
      std::string currentCaStateString;
      CSA::csaState nextCsaState;
      std::string csaStateString;
      std::set<std::string> csaStateStringSet;
      Regexp::Derivatives::caState nextCaState;
      std::string nextCsaStateString;
      std::set<Regexp::Derivatives::counterGuard> counterGuards;
      // We need to decide whether any transition was taken for specified bytemap or not
      // When there is no counter guard in scope but transition was taken, the minterms will contain True
      // When there is no counter guard in scope and transition was not taken, the minterms will be empty as there are
      // no transitions to take
      bool transitionTaken = false;
      // All ca states from the current csa state must be explored
      // All counter guards of transitions from all ca states must be gathered in order to compute minterms (i.e gamma set)
      for (auto const &caStateToExplore: csaStateToExplore) {
        currentCaStateString = derivatives.numberRegexMapping.at(caStateToExplore.first);
        csaStateString += currentCaStateString + "; ";
        // Empty state (i.e. epsilon) can not have any outgoing transitions
        if (caStateToExplore.first == derivatives.emptyStateNumber) {
          csaStateStringSet.insert(" ");
          continue;
        }
        csaStateStringSet.insert(currentCaStateString);
        outgoingTransitions = derivatives.partialDerivatives[caStateToExplore.first];
        currentCaStateScope = caStateToExplore.second;
        for (auto const &transition: outgoingTransitions) {
          // Only transitions with the same bytemap as the one for which the gamma set is computed are relevant
          if (transition.first.first != bytemap) {
            continue;
          }
          transitionTaken = true;

          // All target ca states will form a new csa state
          nextCaState = derivatives.caStates[transition.first.second];
          if (nextCsaState.count(nextCaState) == 0) {
            nextCsaState.insert(nextCaState);
          }

          // Explore all operators on the transition
          for (auto const &transitionOperator: transition.second.second) {
            // Only operators for counters in scope are relevant
            if (currentCaStateScope.count(transitionOperator.countingLoop) == 1) {
              switch (transitionOperator.op) {
                case Regexp::Derivatives::ID:
                  counterGuards.insert({Regexp::Derivatives::True, transitionOperator.countingLoop, transitionOperator.counting_min, transitionOperator.counting_max, false});
                  break;
                case Regexp::Derivatives::INCR:
                  counterGuards.insert({Regexp::Derivatives::CanIncr, transitionOperator.countingLoop, transitionOperator.counting_min, transitionOperator.counting_max, false});
                  break;
                case Regexp::Derivatives::EXIT:
                case Regexp::Derivatives::EXIT1:
                  counterGuards.insert({Regexp::Derivatives::CanExit, transitionOperator.countingLoop, transitionOperator.counting_min, transitionOperator.counting_max, false});
                  break;
              }
            }
          }
        }
      }
      for (auto const &nextState: nextCsaState) {
        nextCsaStateString += derivatives.numberRegexMapping.at(nextState.first) + "; ";
      }
      csaStateString = "";
      for (auto const &currentState: csaStateStringSet) {
        csaStateString += currentState + "; ";
      }
      // Delete trailing space and semicolon
      csaStateString = csaStateString.substr(0, csaStateString.length()-2);
      nextCsaStateString = nextCsaStateString.substr(0, nextCsaStateString.length()-2);
      // Compute minterms (i.e. gamma set) and save it if it is nonempty
      std::set<std::set<Regexp::Derivatives::counterGuard>> minterms = {};
      std::set<Regexp::Derivatives::counterGuard> tmpMinterm;
      std::set<std::set<counterIntGuard>> mintermsNew = {};
      if (!counterGuards.empty() || transitionTaken) {
        mintermsNew = computeMinterms(counterGuards);
        for (auto const &a: mintermsNew) {
          tmpMinterm.clear();
          for (auto const &b: a) {
            tmpMinterm.insert({b.grd, b.countingLoop, b.countingLoopMin, b.countingLoopMax, b.logical_not});
          }
          minterms.insert(tmpMinterm);
        }
        if (this->gammaSets.count(csaStateString) == 0) {
          this->gammaSets.insert({csaStateString, {{bytemap, minterms}}});
        } else {
          this->gammaSets.at(csaStateString).insert({{bytemap, minterms}});
        }
      }
      if (!nextCsaState.empty()) {
        this->csaStates.insert({nextCsaStateString, nextCsaState});
        if (this->alreadyFoundCsaStates.count(nextCsaState) == 0) {
          this->csaStatesToExplore.emplace_back(nextCsaState, nextCsaStateString);
        }
      }
    }

    void CSA::printGammaSet() {
      // In the map, the keys will be sorted (for testing)
      std::map<std::string, std::unordered_map<int, std::set<std::set<Regexp::Derivatives::counterGuard>>>> sortedGamma(
              this->gammaSets.begin(), this->gammaSets.end());
      std::string currentGuardSet;
      for (auto const &value: sortedGamma) {
        std::cout << "State: " << value.first << std::endl;
        // To be consistent in output order (for testing)
        std::map<int, std::set<std::set<Regexp::Derivatives::counterGuard>>> sortedcounterGuardsMap(value.second.begin(), value.second.end());
        for (auto const &secMapValue: sortedcounterGuardsMap) {
          std::cout << "Bytemap: " << secMapValue.first;
          for (auto const &guardSet: secMapValue.second) {
            std::cout << ", Guard set: ";
            currentGuardSet = "";
            for (auto const &guard: guardSet) {
              if (guard.logical_not) {
                currentGuardSet += "not ";
              }
              currentGuardSet += getGuardTypeString(guard.grd);
              if (guard.countingLoop != 0) {
                currentGuardSet += " " + this->countingLoopStrings[guard.countingLoop];
              }
              currentGuardSet += ", ";
            }
            currentGuardSet.pop_back();
            currentGuardSet.pop_back();
            std::cout << currentGuardSet;
          }
          std::cout << std::endl;
        }
      }
    }

    std::string CSA::getGuardTypeString(Regexp::Derivatives::counterGuardEnum guardType) {
      switch (guardType) {
        case Regexp::Derivatives::True:
          return "True";
        case Regexp::Derivatives::CanIncr:
          return "CanIncr";
        case Regexp::Derivatives::CanExit:
          return "CanExit";
        case Regexp::Derivatives::False:
          return "False";
      }
      return "NOT KNOW TYPE";
    }

    std::string CSA::getCsaOpTypeString(CSA::setCounterOperatorEnum opType) {
      switch (opType) {
        case CSA::S_ID:
          return "S_ID";
        case CSA::S_INCR:
          return "S_INCR";
        case CSA::S_RST:
          return "S_RST";
        case CSA::S_RST1:
          return "S_RST1";
      }
      return "NOT KNOW TYPE";
    }

    void CSA::printMinterms(const std::set<std::set<Regexp::Derivatives::counterGuard>>& minterms) {
      for (auto const &a: minterms) {
        std::cout << "SET" << std::endl;
        for (auto const &b: a) {
          std::cout << getGuardTypeString(b.grd) << " " << b.countingLoop << " " << b.logical_not << ", ";
        }
        std::cout << std::endl;
      }
    }

    // Transitions have to be sorted in order to automatically compare results of the program with reference results
    bool sortTransitions(const CSA::csaTransition& a,
                         const CSA::csaTransition& b) {
      // We must compare only the string part of the target state tuple
      if (std::get<1>(std::get<4>(a)) != std::get<1>(std::get<4>(b))) {
        return (std::get<1>(std::get<4>(a)) < std::get<1>(std::get<4>(b)));
      } else if (std::get<2>(a) != std::get<2>(b)) {
        return (std::get<3>(a) < std::get<3>(b));
      } else if (std::get<1>(a) != std::get<1>(b)) {
        return (std::get<1>(a) < std::get<1>(b));
      }
      if (std::get<1>(std::get<4>(a)).empty()) {
        return true;
      }
      return (std::get<1>(std::get<4>(a)) > std::get<1>(std::get<4>(b)));
    }

    void CSA::printBetterCountingSetAutomaton(const Regexp::Derivatives& derivatives, bool printCompressedStates) {
#if defined(DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS)
      for (auto const &a: this->stateIntMap) {
        this->intStateMap.insert({a.second, a.first});
      }
      this->intStateMap.insert({0, ""});
      // Map is sorted, for testing
      std::unordered_map<std::string, std::unordered_map<int, std::vector<CSA::csaTransition>>> tmpAuto;
      int currentStateInt = 0;
      int currentBytemap = 0;
      std::vector<CSA::csaTransition> newTransitions;
      for (auto const &state: this->betterCountingSetAutomaton) {
        if (state.empty()) {
          currentStateInt++;
          continue;
        }
        currentBytemap = 0;
        for (auto const &bytemapTransitions: state) {
          newTransitions.clear();
          for (auto const &transitions: std::get<1>(bytemapTransitions)) {
            CSA::csaState emptyCsaState = {};
            std::tuple<CSA::csaState, std::string> a = {emptyCsaState, this->intStateMap.at(currentStateInt)};
            std::set<CSA::setCounterOperator> tmpCounterOperator;
            std::set<Regexp::Derivatives::counterGuard> tmpCounterGuard;
            for (auto const &i: std::get<0>(transitions)) {
              tmpCounterOperator.insert({i.op, i.countingLoop, i.counting_min, i.counting_max});
            }
            for (auto const &i: std::get<1>(transitions)) {
              tmpCounterGuard.insert({i.grd, i.countingLoop, i.countingLoopMin, i.countingLoopMax, i.logical_not});
            }
            if (std::get<2>(transitions) == -1) {
              continue;
            };
            newTransitions.push_back({{emptyCsaState, this->intStateMap.at(currentStateInt)}, currentBytemap, tmpCounterOperator, tmpCounterGuard, {emptyCsaState, this->intStateMap.at(std::get<2>(transitions))}});
          }
          if (tmpAuto.count(this->intStateMap.at(currentStateInt)) == 1) {
            tmpAuto.at(this->intStateMap.at(currentStateInt)).insert({currentBytemap, newTransitions});
          } else {
            tmpAuto.insert({this->intStateMap.at(currentStateInt), {{currentBytemap, newTransitions}}});
          }
          currentBytemap++;
        }
        currentStateInt++;
      }
      if (printCompressedStates) {
        std::map<unsigned int, std::string> tmpCompressedStates;
        if (!derivatives.compressedStates.empty()) {
          tmpCompressedStates = derivatives.compressedStates;
        } else {
          // In the map, the keys will be sorted (for testing)
          std::map<std::string, std::vector<Regexp::Derivatives::caTransition>> sortedDerivatives;
          for (unsigned int i = 0; i < derivatives.partialDerivatives.size(); i++) {
            if (derivatives.partialDerivatives[i].empty()) {
              continue;
            }
            sortedDerivatives.insert({derivatives.numberRegexMapping.at(i), derivatives.partialDerivatives[i]});
          }
          // We create a map between the original state strings and a new compressed string
          if (printCompressedStates) {
            int i = 0;
            for (auto &it: sortedDerivatives) {
              tmpCompressedStates.insert({derivatives.regexNumberMapping.at(it.first), "q" + std::to_string(i)});
              i++;
            }
            // we must also add the empty state, which is not in the keys of the sorted derivatives
            tmpCompressedStates.insert({derivatives.emptyStateNumber, "q" + std::to_string(i)});
            this->compressedStates.insert({"", "q" + std::to_string(i)});
          }
        }
        for (auto const &state: this->csaStates) {
          std::string tmpCompressedCsaState;
          std::set<std::string> tmpCsaStateStringSet;
          for (auto const &caState: state.second) {
            tmpCsaStateStringSet.insert(derivatives.numberRegexMapping.at(caState.first));
          }
          for (auto const &caStateString: tmpCsaStateStringSet) {
            tmpCompressedCsaState += tmpCompressedStates.at(derivatives.regexNumberMapping.at(caStateString)) + ";";
          }
          tmpCompressedCsaState = tmpCompressedCsaState.substr(0, tmpCompressedCsaState.size()-1);
          this->compressedStates.insert({state.first, tmpCompressedCsaState});
        }
      }
      // Map is sorted, for testing
      std::map<std::string, std::unordered_map<int, std::vector<CSA::csaTransition>>> sortedAutomaton(
              tmpAuto.begin(), tmpAuto.end());
      for (auto &outgoingTransitions: sortedAutomaton) {
        // The first is the the key, i.e., the source state
        if (printCompressedStates) {
          std::cout << "SOURCE STATE: " << this->compressedStates.at(outgoingTransitions.first) << std::endl;
        } else {
          std::cout << "SOURCE STATE: " << outgoingTransitions.first << std::endl;
        }
        std::vector<CSA::csaTransition> outgoingTransitionsForAllBytemaps;
        for (auto const &bytemapTransitionMapElement: outgoingTransitions.second) {
          for (auto const &transitionElement: bytemapTransitionMapElement.second) {
            outgoingTransitionsForAllBytemaps.push_back(transitionElement);
          }
        }
        std::sort(outgoingTransitionsForAllBytemaps.begin(), outgoingTransitionsForAllBytemaps.end(), sortTransitions);
        for (auto const &outgoingTransition: outgoingTransitionsForAllBytemaps) {
          std::cout << "Bytemap " << std::get<1>(outgoingTransition);
          if (!std::get<2>(outgoingTransition).empty()) {
            std::cout << ", OPS: ";
            for (auto const &setOperator: std::get<2>(outgoingTransition)) {
              std::cout << this->getCsaOpTypeString(setOperator.op) << " " << this->countingLoopStrings[setOperator.countingLoop] << ", ";
            }
          } else {
            std::cout << ", ";
          }
          std::cout << "GUARDS: ";
          for (auto const &guard: std::get<3>(outgoingTransition)) {
            if (guard.logical_not) {
              std::cout << "NOT " << this->getGuardTypeString(guard.grd);
              if (guard.grd != Regexp::Derivatives::True) {
                std::cout << " " << this->countingLoopStrings[guard.countingLoop];
              }
              std::cout << ", ";
            } else {
              std::cout << this->getGuardTypeString(guard.grd);
              if (guard.grd != Regexp::Derivatives::True) {
                std::cout << " " << this->countingLoopStrings[guard.countingLoop];
              }
              std::cout << ", ";
            }
          }
          if (printCompressedStates) {
            std::cout << this->compressedStates.at(std::get<1>(std::get<4>(outgoingTransition))) << std::endl;
          } else {
            std::cout << std::get<1>(std::get<4>(outgoingTransition)) << std::endl;
          }
        }
      }
#else
      std::cout << "PRINTING NOT AVAILABLE WITHOUT DETERMINIZATIONTESTS or CSAFINALSTATESTESTS flag defined)" << std::endl;
#endif
    }

    void CSA::printCompressedStates() {
      for (auto const &state: this->compressedStates) {
        std::cout << state.second << " " << state.first << std::endl;
      }
    }

    void CSA::printCsaStates() {
      for (auto const &csaState: this->csaStates) {
        std::cout << csaState.first << std::endl;
      }
    }

    void CSA::printFinalStates() {
      for (auto const &a: this->stateIntMap) {
        this->intStateMap.insert({a.second, a.first});
      }
      std::string guards;
      // Map is sorted, for testing
      std::map<std::string, std::set<Regexp::Derivatives::finalStateCondition>> tmpFinalStates;
      std::set<Regexp::Derivatives::finalStateCondition> tmpConditionSet;
      for (unsigned long i = 0; i < this->finalStates.size(); i++) {
        tmpConditionSet.clear();
        if (this->finalStates[i].empty()) {
          continue;
        }
        for (auto const b: this->finalStates[i]) {
          tmpConditionSet.insert({b.grd, b.countingLoop, b.counting_min, b.counting_max});
        }
        tmpFinalStates.insert({this->intStateMap.at((int)i), tmpConditionSet});
      }
      std::map<std::string, std::set<Regexp::Derivatives::finalStateCondition>> sortedFinalStates(
              tmpFinalStates.begin(), tmpFinalStates.end());
      for (auto const &finalState: sortedFinalStates) {
        if (finalState.second.size() == 1 && finalState.second.begin()->grd == Regexp::Derivatives::False) {
          continue;
        }
        std::cout << "STATE: " << finalState.first << ", Guard: ";
        guards = "";
        for (auto const &finalStateGuard: finalState.second) {
          if (finalStateGuard.grd == Regexp::Derivatives::CanExit) {
            guards += "CanExit, Counting loop: " + this->countingLoopStrings[finalStateGuard.countingLoop] + ", ";
          } else if (finalStateGuard.grd == Regexp::Derivatives::True) {
            guards += "True, ";
          } else {
            guards += "UNKNOWN FINAL STATE GUARD, ";
          }
        }
        // Do not print trailing comma
        std::cout << guards.substr(0, guards.size()-2) << std::endl;
      }
    }

    void CSA::printCountingSetMemory() {
      std::string values;
      for (unsigned long i = 0; i < this->countingSetMemory.size(); i++) {
        std::cout << "COUNTER: " << this->countingLoopStrings[(int)i] << ", OFFSET: " << this->countingSetMemory[i].first << std::endl;
        std::cout << "\tValues: ";
        values = "";
        for (int const &counterValue: this->countingSetMemory[(int)i].second) {
          values += std::to_string(counterValue) + ", ";
        }
        std::cout << values.substr(0, values.size()-2) << std::endl;
      }
    }
}
