//
// Created by Michal Horký (xhorky23) on 21.02.21.
//

#ifndef RE2_CSA_H
#define RE2_CSA_H

#include "re2/regexp.h"
#include "re2/derivatives.h"

#include <unordered_map>
#include <vector>
#include <list>
#include <iostream>
#include <deque>

namespace re2 {
    class CSA {
    public:
        // Operations for CsA
        enum setCounterOperatorEnum {
            S_ID,
            S_INCR,
            S_RST1,
            S_RST,
        };

        // Conditions of counters used for more efficient storage of outgoing transition
        enum countingConditions {
            // All values of the counter are below zero
            LOW = 1,
            // When the counter not satisfy LOW and HIGH
            MIDDLE = 2,
            // Singleton set containing the upper bound of the counter
            HIGH = 3,
        };

        // Besides the operator itself, the counting loop must be saved
        struct setCounterOperator {
            CSA::setCounterOperatorEnum op;
            unsigned int countingLoop;
            int counting_min;
            int counting_max;

            // Needed for set
            bool operator<(const setCounterOperator &other) const {
              if (op == other.op) {
                if (countingLoop == other.countingLoop) {
                  if (counting_min == other.counting_min) {
                    return counting_max < other.counting_max;
                  }
                  return counting_min < other.counting_min;
                }
                return countingLoop < other.countingLoop;
              }
              return op < other.op;
            }

            bool operator==(const setCounterOperator &other) const {
              if (op == other.op) {
                if (countingLoop == other.countingLoop) {
                  if (counting_min == other.counting_min) {
                    return counting_max == other.counting_max;
                  }
                }
              }
              return false;
            }
        };

        // Besides the guard itself, it must be saved if logical not is used or not
        struct counterIntGuard {
            Regexp::Derivatives::counterGuardEnum grd;
            unsigned int countingLoop;
            int countingLoopMin;
            int countingLoopMax;
            bool logical_not;

            // Original < operator used for ordering od counter guards in the output
            // It was later changed for some optimizations, but remains for the tests and determinization
#if defined(SCOPETESTS) || defined(GAMMASETTESTS) || defined(DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS)
            bool operator<(const counterIntGuard &other) const {
              if (grd == other.grd) {
                if (countingLoop == other.countingLoop) {
                  return logical_not < other.logical_not;
                }
                return countingLoop < other.countingLoop;
              }
              return grd < other.grd;
            }
#else
            bool operator<(const counterIntGuard &other) const {
              if (countingLoop == other.countingLoop) {
                if (grd == other.grd) {
                  return logical_not < other.logical_not;
                }
                return grd < other.grd;
              }
              return countingLoop < other.countingLoop;
            }
#endif

            bool operator==(const counterIntGuard &other) const {
              return (countingLoop == other.countingLoop && grd == other.grd && logical_not == other.logical_not);
            };
        };

        // Conditions for final state, if they are not satisfied the state is not final
        struct finalStateConditionInt {
            Regexp::Derivatives::counterGuardEnum grd;
            unsigned int countingLoop;
            int counting_min;
            int counting_max;

            bool isSet;

            // Needed for set
            bool operator<(const finalStateConditionInt &other) const {
              if (grd == other.grd) {
                return countingLoop < other.countingLoop;
              }
              return grd < other.grd;
            }
        };

        typedef std::set<Regexp::Derivatives::caState> csaState;
        // Old transition, just for tests
        typedef std::tuple<std::tuple<CSA::csaState, std::string>, int, std::set<CSA::setCounterOperator>, std::set<Regexp::Derivatives::counterGuard>, std::tuple<CSA::csaState, std::string>> csaTransition;

        std::vector<std::set<finalStateConditionInt>> finalStates;
        unsigned int initialState;
        std::string initialStateString;


        // To use the original determinization test (before optimizations), where the guard is part of the transition
#if defined(DETERMINIZATIONTESTS) || defined(CSAFINALSTATESTESTS)
        typedef std::tuple<std::set<CSA::setCounterOperator>, std::set<counterIntGuard>, int> betterTransition;
#else
        typedef std::pair<std::set<CSA::setCounterOperator>, int> betterTransition;
#endif


        // The index of the first vector is the bytemap class. The vector in the pair is the vector of used counters
        // The vector in the pair is vector of transitions
        typedef std::vector<std::pair<std::vector<unsigned int>, std::vector<betterTransition>>> vectorArrayType;

        int stateCounter;

        // To map the states to their number representation, so they can be used as indexes in the final CsA structure
        std::unordered_map<std::string, int> stateIntMap;
        std::unordered_map<int, std::string> intStateMap;

        // Index of the vector is the state number
        std::vector<vectorArrayType> betterCountingSetAutomaton;

        std::unordered_map<std::string, CSA::csaState> csaStates;
        std::set<CSA::csaState> alreadyFoundCsaStates;
        std::vector<std::pair<CSA::csaState, std::string>> csaStatesToExplore;

        // A state is represented by the string, since std::set can not be a key of unordered map
        // Used only for tests
        std::unordered_map<std::string, std::unordered_map<int, std::set<std::set<Regexp::Derivatives::counterGuard>>>> gammaSets;

        unsigned int countingLoopCounter;
        std::vector<std::pair<int, int>> countingLoopBounds;
        std::vector<std::string> countingLoopStrings;

        // Memory used during the matching, first component of the pair is the offset, the second is queue of strictly
        // increasing integers
        std::vector<std::pair<int, std::deque<int>>> countingSetMemory;
        std::unordered_set<unsigned int> countingLoops;
        Regexp::Derivatives derivatives_var;
        std::vector<std::vector<bool>> stateBytemapComputedVector;
        std::vector<std::pair<CSA::csaState, std::string>> intCsaStatePairVector;
        // Save the bytemap range as class variable
        int bytemapRange_var;

        // just an interface for determinization of CA
        void getCsa(Regexp::Derivatives &derivatives, re2::Regexp * initialRegexp, int bytemapRange);

        // Matching interface
        bool match(const StringPiece& text, const uint8_t* bytemap);

        // For tests
        void printBetterCountingSetAutomaton(const Regexp::Derivatives& derivatives, bool printCompressedStates=true);
        // We create a map between the original state strings and a new compressed string
        std::map<std::string, std::string> compressedStates;
        void printCsaStates();
        void printFinalStates();
        void printCompressedStates();

    private:
        // to compute the scope of individual states, the scope is a part of each state, see derivatives.h
        void computeStateScopes(Regexp::Derivatives &derivatives);

        // Deleting transitions that can never be taken
        static void preprocessCa(Regexp::Derivatives &derivatives);

        // Method that gets the outgoing transitions for the state and the bytemap
        void getNextStateAndTransitions(const std::pair<CSA::csaState, std::string>& csaStateToExplorePair, int bytemap);

        // Method that gets relevant counter guards
        static bool getCounterGuards(const CSA::csaState& csaStateToExplore, int bytemap, Regexp::Derivatives derivatives, std::set<Regexp::Derivatives::counterGuard> &counterGuards, std::vector<unsigned int> &usedCounters);

        // Method gets the transition operator for newly created CsA transition
        static void getCsaTransitionOperator(const Regexp::Derivatives::caTransition& transition, const std::set<unsigned int>& currentCaStateScope, const std::set<unsigned int>& targetCaStateScope, std::set<CSA::setCounterOperator> &csaTransitionOperators);

        // Method does the update of the counter memory based on the operations of the taken transition
        void updateMemory(const std::set<CSA::setCounterOperator>& operations);

        // Used for the optimization, it gets the appropriate index to save the outgoing transition
        static std::vector<unsigned long> computeGuardIndexes(std::vector<counterIntGuard> guardscomputeGuardIndexes);

        // It gets the number representing LOW, MIDDLE, HIGH or its combination
        static std::pair<unsigned long, unsigned long> getGuardNumber(counterIntGuard firstGuard, counterIntGuard secondGuard = {}, bool twoGuards = false);

        // Update the indexes for the outgoing transition during its computation
        static void updateIndexes(std::vector<unsigned long> &result, int countingLoop, unsigned long firstGuardInt, unsigned long secondGuardInt = 0);

        // Gets the index of the outgoing transition based on the current state of the counter memory
        unsigned long getVectorIndexFromUsedCounters(const std::vector<unsigned int>& usedCounters);

        // When compiled with GAMMASETTESTS it also creates gamma set
        void updateGammaSet(const CSA::csaState& csaStateToExplore, int bytemap, Regexp::Derivatives derivatives);

        // Checks satisfiability of conjunction of two sets of counter guards
        // One of the set must have the logical_not negated for all the guards
        static bool checkSatisfiability(const std::set<Regexp::Derivatives::counterGuard>& firstSet, const std::set<Regexp::Derivatives::counterGuard>& secondSet);

        // Printing functions for testing
        void printGammaSet();
        static void printMinterms(const std::set<std::set<Regexp::Derivatives::counterGuard>>& minterms);
        void printCountingSetMemory();

        static std::string getGuardTypeString(Regexp::Derivatives::counterGuardEnum guardType);
        static std::string getCsaOpTypeString(CSA::setCounterOperatorEnum opType);

        // Computes minters (i.e., the gamma set from the paper)
        static std::set<std::set<counterIntGuard>> computeMinterms(const std::set<Regexp::Derivatives::counterGuard>& counterGuards);

        // Function to get difference of two unordered sets
        static void unordered_set_difference(const std::unordered_set<unsigned int>& first, const std::unordered_set<unsigned int>& second, std::set<unsigned int> &result);

        // Functions to work with the counter memory
        int counterMinimumValue(unsigned int counterInt);
        int counterMaximumValue(unsigned int counterInt);
        void resetCounter(unsigned int counterInt);
        void resetCounterOne(unsigned int counterInt);
        void incrementCounter(unsigned int counterInt, int counterMax);
        void incrementCounterAfterID(unsigned int counterInt, int counterMax);
        void initCounterMemory();
    };
}

#endif //RE2_CSA_H
