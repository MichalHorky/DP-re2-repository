# Author: Michal Horky (xhorky23)
# This script runs the installation script (myMake.sh)
# This script must be run as the superuser (due to myMake.sh)
cd re2-master && ./makeScript.sh
